# Desarrollo

El proyecto usa [docker](https://docs.docker.com/get-docker/) y docker-compose para ejecutar la aplicación localmente.
Para ejecutar la aplicación se debe correr docker-compose en el directorio raiz del proyecto:

```shell script
$ docker-compose up -d
```
