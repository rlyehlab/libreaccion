#!/bin/sh
npm install || exit 1

if [ -z "$SERVER_BASE_URL" ]; then
  export SERVER_BASE_URL="http://localhost:9000"
fi

npx webpack --watch \
  --config src/main/frontend/webpack.config.js \
  --host 0.0.0.0
