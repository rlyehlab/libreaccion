FROM libreaccion-dev AS build
USER root
WORKDIR /build
COPY pom.xml /build
RUN mvn verify

## Builds frontend
COPY package.json /build/package.json
COPY package-lock.json /build/package-lock.json
COPY src/main/frontend /build/src/main/frontend
RUN npm install
RUN npx webpack --mode=production --config src/main/frontend/webpack.config.js

## Builds backend
COPY . /build
RUN mvn install
RUN mvn assembly:single

# Multi-stage build, the final image only contains the generated jar
FROM openjdk:11-jre-slim
COPY --from=build /build/target/libreaccion-jar-with-dependencies.jar /usr/share/app/app.jar

RUN useradd \
        --user-group \
        --no-create-home \
        --uid 1000 \
        --shell /bin/false \
        app

# Drop privs
USER app
WORKDIR /usr/share/app/
ENTRYPOINT ["java", "-jar", "/usr/share/app/app.jar"]
