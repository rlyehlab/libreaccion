package net.libreaccion.util

import net.libreaccion.datasource.DataSourceConfig
import net.libreaccion.domain.accounts.persistence.AccountOrganizations
import net.libreaccion.domain.accounts.persistence.Accounts
import net.libreaccion.domain.accounts.persistence.Organizations
import net.libreaccion.domain.accounts.persistence.Profiles
import net.libreaccion.domain.campaigns.persistence.Campaigns
import net.libreaccion.domain.campaigns.persistence.CampaignsTargets
import net.libreaccion.domain.campaigns.persistence.Targets
import net.libreaccion.domain.endorsements.persistence.Endorsements
import net.libreaccion.domain.sessions.persistence.Sessions
import net.libreaccion.domain.store.persistence.Documents
import net.libreaccion.domain.store.persistence.Locks
import net.libreaccion.support.persistence.DataSourceInitializer
import net.libreaccion.support.persistence.TransactionSupport
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.test.util.ReflectionTestUtils

object TestDataSource {
    private val config: DataSourceConfig = DataSourceConfig(
        url = "jdbc:h2:file:./data/db/test_db",
        user = "sa",
        password = "",
        driver = "org.h2.Driver",
        logStatements = true,
        drop = "false"
    )

    val db: Database by lazy {
        Database.connect(
            url = config.url,
            user = config.user,
            password = config.password,
            driver = config.driver
        )
    }

    val initializer = DataSourceInitializer(
        dataSourceConfig = config,
        migrations = emptyList(),
        tables = listOf(
            Accounts,
            Organizations,
            AccountOrganizations,
            Profiles,
            Campaigns,
            Targets,
            CampaignsTargets,
            Documents,
            Locks,
            Sessions,
            Endorsements
        )
    )

    init {
        ReflectionTestUtils.setField(initializer, "db", db)
        dropAll()
    }

    fun dropAll() {
        transaction(db) {
            exec("DROP ALL OBJECTS")
            initializer.initAuto()
        }
    }

    fun<T : TransactionSupport> initTransaction(transactionSupport: T): T {
        ReflectionTestUtils.setField(transactionSupport, "db", db)
        return transactionSupport
    }

    fun transaction(statement: Transaction.() -> Unit) {
        org.jetbrains.exposed.sql.transactions.transaction(db) {
            addLogger(StdOutSqlLogger)
            statement()
        }
    }
}
