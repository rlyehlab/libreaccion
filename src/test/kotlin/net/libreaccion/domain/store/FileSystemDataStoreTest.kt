package net.libreaccion.domain.store

import net.libreaccion.domain.store.HashUtils.hash
import net.libreaccion.domain.store.model.Document
import net.libreaccion.domain.store.persistence.DocumentDAO
import net.libreaccion.util.TestDataSource.initTransaction
import org.junit.AfterClass
import org.junit.Test
import java.io.File

class FileSystemDataStoreTest {

    private val documentDAO: DocumentDAO by lazy {
        initTransaction(DocumentDAO())
    }

    private val store = FileSystemDataStore(documentDAO, directory)
    private val logo = File("src/main/frontend/assets/images/logo.svg")

    companion object {
        private val directory: File = createTempDir("fsds-", "-test")

        @AfterClass
        fun tearDown() {
            directory.deleteRecursively()
        }
    }

    @Test
    fun storeAndRead() {
        val document1: Document = storeLogo()
        val path1: File = store.resolveDocumentPath(document1)
        assert(path1.exists())
        assert(path1.length() == logo.length())
        assert(store.findById(document1.id) == document1)
        assert(store.get(document1.id) == document1)
        assert(hash(store.read(document1.id)) == document1.id)

        store.delete(document1.id)
        assert(store.findById(document1.id) == null)
        assert(store.get(document1.id) == document1.delete())
    }

    @Test(expected = IllegalArgumentException::class)
    fun delete() {
        val document: Document = storeLogo()
        assert(hash(store.read(document)) == document.id)
        store.delete(document.id)
        store.read(document)
    }

    private fun storeLogo(): Document {
        return store.save(Document.new(
            name = "logo.svg",
            type = "image/svg+xml",
            size = logo.length(),
            meta = emptyMap()
        ), logo.inputStream())
    }
}
