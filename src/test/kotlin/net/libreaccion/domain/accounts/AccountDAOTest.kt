package net.libreaccion.domain.accounts

import net.libreaccion.domain.accounts.model.Account
import net.libreaccion.domain.accounts.model.Organization
import net.libreaccion.domain.accounts.model.Role
import net.libreaccion.security.Roles
import net.libreaccion.domain.accounts.persistence.AccountDAO
import net.libreaccion.domain.accounts.persistence.OrganizationDAO
import net.libreaccion.util.TestDataSource
import org.junit.Before
import org.junit.Test

class AccountDAOTest {

    private val accountDAO: AccountDAO by lazy {
        TestDataSource.initTransaction(AccountDAO())
    }
    private val organizationDAO: OrganizationDAO by lazy {
        TestDataSource.initTransaction(OrganizationDAO())
    }

    @Before
    fun setUp() {
        TestDataSource.dropAll()
    }

    @Test
    fun findByEmail() {
        val newAccount = accountDAO.saveOrUpdate(
            Account.new(
                organizations = emptyList(),
                email = "foo@bar.net",
                password = "test pwd"
            )
        )
        assert(accountDAO.findByEmail("foo@bar.net") == newAccount)
    }

    @Test
    fun assignRoles() {
        val lab = organizationDAO.saveOrUpdate(Organization.new(
            name = "Rlyeh Lab",
            contactEmail = "contacto@rlab.be",
            public = true
        ))
        val newAccount = accountDAO.saveOrUpdate(
            Account.new(
                organizations = listOf(lab),
                email = "foo@bar.net",
                password = "test pwd"
            )
        )
        assert(accountDAO.getRoles(newAccount, lab).isEmpty())
        accountDAO.assignRoles(newAccount, lab, listOf(Roles.ADMIN))

        val resolvedRoles: List<Role> = accountDAO.getRoles(newAccount, lab)
        assert(resolvedRoles.size == 1)
        assert(resolvedRoles[0] == Roles.ADMIN)
    }
}