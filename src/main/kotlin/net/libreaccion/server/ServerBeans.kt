package net.libreaccion.server

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import net.libreaccion.server.SessionDataStore.Companion.SESSION_COOKIE_NAME
import org.eclipse.jetty.server.session.NullSessionCache
import org.eclipse.jetty.server.session.SessionHandler
import org.springframework.context.support.BeanDefinitionDsl
import org.springframework.context.support.beans

object ServerBeans {
    fun beans() = beans {
        val mainConfig: Config = ConfigFactory.defaultApplication().resolve()

        // Session management
        bean {
            SessionDataStore(
                ttlInSeconds = mainConfig.getInt("app.server.sessionTtlInSeconds"),
                sessionDAO = ref()
            )
        }

        // Javalin does not support registering the same session handler twice in different
        // ServletContextHandler, so we need to create a new instance for each handler.
        bean {
            SessionHandler().apply {
                sessionCache = NullSessionCache(this).apply {
                    sessionDataStore = ref()
                }
                sessionCookieConfig.name = SESSION_COOKIE_NAME
            }
        }
    }
}
