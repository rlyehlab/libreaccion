package net.libreaccion.server

data class ServerConfig(
    val host: String,
    val port: Int,
    val appName: String,
    val contextPath: String,
    val baseUrl: String
)
