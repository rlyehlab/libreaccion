package net.libreaccion.server

import net.libreaccion.domain.sessions.model.Session
import net.libreaccion.domain.sessions.persistence.SessionDAO
import org.eclipse.jetty.server.session.AbstractSessionDataStore
import org.eclipse.jetty.server.session.SessionData
import org.joda.time.DateTime

class SessionDataStore(
    private val ttlInSeconds: Int,
    private val sessionDAO: SessionDAO
) : AbstractSessionDataStore() {

    companion object {
        const val SESSION_COOKIE_NAME: String = "sid"
    }

    override fun delete(id: String): Boolean {
        return sessionDAO.delete(id)
    }

    override fun isPassivating(): Boolean {
        return false
    }

    override fun exists(id: String): Boolean {
        return sessionDAO.exists(id)
    }

    override fun doStore(
        id: String,
        data: SessionData,
        lastSaveTime: Long
    ) {
        val session: Session = sessionDAO.find(id) ?: Session.new(
            id = id,
            contextPath = data.contextPath,
            vhost = data.vhost,
            created = data.created,
            accessed = data.accessed,
            lastAccessed = data.lastAccessed,
            maxInactiveMs = data.maxInactiveMs,
            expiration = DateTime.now().plusSeconds(ttlInSeconds)
        )
        sessionDAO.saveOrUpdate(session.withAttributes(data.allAttributes))
    }

    override fun doLoad(id: String): SessionData? {
        return sessionDAO.find(id)?.toSessionData()
    }

    override fun doGetExpired(candidates: Set<String>): Set<String> {
        return sessionDAO.findAll(candidates).map(Session::id).toSet()
    }
}
