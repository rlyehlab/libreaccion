package net.libreaccion

object Environment {
    const val LOCAL_DEV = "localdev"
    const val PROD = "production"
    const val TEST = "test"

    private const val ENV_NAME: String = "JAVA_ENV"
    val currentEnv: String = System.getenv(ENV_NAME) ?: LOCAL_DEV
    val isLocalDev: Boolean = currentEnv == LOCAL_DEV
    val isProd: Boolean = currentEnv == PROD
}
