package net.libreaccion.support

import com.amirkhawaja.Ksuid
import com.amirkhawaja.KsuidComponents

object IdGenerator {
    private val ksuid = Ksuid()

    fun randomId(): String {
        return ksuid.generate()
    }

    fun parseId(id: String): KsuidComponents {
        return ksuid.parse(id)
    }
}