package net.libreaccion.support.pagination

import io.javalin.http.Context
import org.jetbrains.exposed.sql.SortOrder
import javax.inject.Inject

/** Must be extended by commands that requires pagination support.
 *
 * It provides a convenient #paging() function to abstract the cursor deserialization and
 * the page result serialization.
 */
abstract class PagingCommand {
    companion object {
        private const val DEFAULT_LIMIT: Int = 30
    }

    @Inject
    private lateinit var pageResultFactory: PageResultFactory

    /** Parses a request and constructs a cursor, then it calls the handler function
     * to retrieve the page described by the cursor. If the handler returns a valid page,
     * it sends the page back as JSON.
     *
     * @param ctx Current request context.
     * @param handler Function to retrieve the underlying page.
     */
    fun<T> paging(
        ctx: Context,
        handler: (Cursor) -> Page<T>
    ) {
        val cursor = Cursor.new(
            offset = ctx.queryParam("offset")?.toLong() ?: 0,
            limit = ctx.queryParam("limit")?.toInt() ?: DEFAULT_LIMIT,
            order = ctx.queryParam("order")?.let {
                order -> SortOrder.valueOf(order.toUpperCase())
            } ?: SortOrder.ASC
        )
        ctx.json(pageResultFactory.create(ctx, handler(cursor)))
    }
}
