package net.libreaccion.support.pagination

import org.jetbrains.exposed.sql.SortOrder

/** Represents the current state of a paginated result.
 */
data class Cursor(
    /** Number of elements on a page */
    val limit: Int,
    /** Position of the first element in the result set. */
    val offset: Long,
    /** Sort order, default is ASC. */
    val order: SortOrder
) {
    companion object {
        fun new(
            offset: Long,
            limit: Int,
            order: SortOrder = SortOrder.ASC
        ): Cursor = Cursor(
            limit = limit,
            offset = offset,
            order = order
        )
    }

    fun next(tailSize: Int = limit): Cursor =
        copy(offset = offset + tailSize)
}
