package net.libreaccion.support.pagination

/** Represents a generic API paginated result.
 */
data class PageDTO<T>(
    /** URI to fetch the previous page. */
    val previous: String?,
    /** URI to fetch the next page */
    val next: String?,
    /** Page elements */
    val items: List<T>,
    /** Total amount of results in the entire result set. */
    val total: Long
)
