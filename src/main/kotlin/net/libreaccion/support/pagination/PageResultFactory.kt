package net.libreaccion.support.pagination

import io.javalin.http.Context

/** Factory to create paginated results.
 */
class PageResultFactory {
    /** Maps a domain Page to the PageDTO.
     *
     * The cursor to retrieve the previous and next pages are generated using the pathname part of the
     * request. It doesn't include the origin.
     *
     * @param ctx Request context to retrieve the pathname.
     * @param page Domain Page to convert.
     */
    fun<T> create(
        ctx: Context,
        page: Page<T>
    ): PageDTO<T> {
        val servicePath: String = ctx.matchedPath()
        val previousPage: String? = page.cursor.takeIf { cursor ->
            cursor.offset > cursor.limit
        }?.let {
            buildPath(servicePath, page.cursor.limit, page.cursor.offset - page.cursor.limit)
        }
        val nextPage: String? = page.items.takeIf { items ->
            page.cursor.offset + items.size < page.total
        }?.let {
            buildPath(servicePath, page.cursor.limit, page.cursor.offset + page.cursor.limit)
        }

        return PageDTO(
            items = page.items,
            previous = previousPage,
            next = nextPage,
            total = page.total
        )
    }

    private fun buildPath(
        servicePath: String,
        limit: Int,
        offset: Long
    ): String =
        "${servicePath.removeSuffix("/")}?offset=$offset&limit=$limit"
}
