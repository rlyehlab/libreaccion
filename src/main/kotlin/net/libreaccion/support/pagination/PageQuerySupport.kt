package net.libreaccion.support.pagination

import net.libreaccion.support.persistence.AbstractEntity
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.SizedIterable

/** Returns a single page from the record set.
 * @param cursor The cursor to describe the page to retrieve.
 * @param table Table to read records from.
 */
fun<T> SizedIterable<T>.paging(
    cursor: Cursor,
    table: IdTable<String>
): SizedIterable<T> {
    return limit(cursor.limit, cursor.offset).orderBy(table.id to cursor.order)
}

/** Converts a paginated result set to a page.
 * @param cursor Current cursor.
 */
fun<T : AbstractEntity<R>, R> SizedIterable<T>.toPage(cursor: Cursor): Page<R> {
    val items: List<R> = map { item ->
        item.toDomainType()
    }
    return Page(items, count(), cursor)
}
