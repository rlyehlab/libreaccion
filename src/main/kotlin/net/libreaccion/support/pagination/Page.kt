package net.libreaccion.support.pagination

/** Represents a generic API list result with pagination.
 */
data class Page<T>(
    /** Page elements */
    val items: List<T>,
    /** Total amount of results in the entire result set. */
    val total: Long,
    /** Cursor at the current position. */
    val cursor: Cursor
) {
    /** Maps all items from this page and returns a new page with the
     * transformed items.
     * @return the page with the new items.
     */
    fun<R> map(predicate: (T) -> R): Page<R> {
        return Page(
            items = items.map(predicate),
            total = total,
            cursor = cursor
        )
    }
}
