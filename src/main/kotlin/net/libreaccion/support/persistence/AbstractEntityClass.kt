package net.libreaccion.support.persistence

import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.exceptions.EntityNotFoundException
import org.jetbrains.exposed.dao.id.IdTable

abstract class AbstractEntityClass<IdType, DomainType, EntityType : AbstractEntity<DomainType>>(
    table: IdTable<String>
) : EntityClass<String, EntityType>(table) {
    fun saveOrUpdate(
        id: IdType,
        source: DomainType
    ): DomainType {
        return try {
            this[id.toString()].update(source)
        } catch (cause: EntityNotFoundException) {
            new(id.toString()) {
                create(source)
            }
        }.toDomainType()
    }
}
