package net.libreaccion.support.persistence

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.id.EntityID

abstract class AbstractEntity<DomainType>(id: EntityID<String>) : Entity<String>(id) {
    abstract fun create(source: DomainType): AbstractEntity<DomainType>
    abstract fun update(source: DomainType): AbstractEntity<DomainType>
    abstract fun toDomainType(): DomainType
}
