package net.libreaccion.support.persistence

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import net.libreaccion.support.ObjectMapperFactory
import java.util.*

object EntitySerialization {
    val objectMapper: ObjectMapper = ObjectMapperFactory.snakeCaseMapper

    fun serialize(entity: Any): String {
        val content = objectMapper.writeValueAsString(entity)
        return Base64.getEncoder().encodeToString(content.toByteArray())
    }

    inline fun <reified T> deserialize(entity: String): T {
        val decodedBytes = Base64.getDecoder().decode(entity)
        return objectMapper.readValue(decodedBytes)
    }
}
