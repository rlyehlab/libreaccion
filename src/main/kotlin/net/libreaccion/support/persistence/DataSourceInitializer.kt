package net.libreaccion.support.persistence

import net.libreaccion.Environment
import net.libreaccion.datasource.DataSourceConfig
import net.libreaccion.datasource.Migration
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.addLogger
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.core.io.support.PathMatchingResourcePatternResolver

class DataSourceInitializer(
    private val dataSourceConfig: DataSourceConfig,
    private val tables: List<Table>,
    private val migrations: List<Migration>
) : TransactionSupport() {

    companion object {
        private const val DROP_ACK: String = "Yes, please delete all my data forver."
    }

    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    val isTest: Boolean = dataSourceConfig.isTest

    fun initAuto(dropSql: String? = null) = transaction {
        if (dataSourceConfig.drop == DROP_ACK && dropSql != null) {
            exec(ClassPathResource(dropSql))
        }
        if (dataSourceConfig.logStatements) {
            addLogger(StdOutSqlLogger)
        }

        SchemaUtils.create(*tables.toTypedArray())
        SchemaUtils.createMissingTablesAndColumns(*tables.toTypedArray())
        runMigrations()
    }

    fun dropIfRequired(dropSql: String) {
        if (dataSourceConfig.drop == DROP_ACK) {
            logger.info("Dropping existing database")
            exec(ClassPathResource(dropSql))
        }
    }

    fun initIfRequired(
        scriptsPath: String
    ) {
        logger.info("Initializing database")

        PathMatchingResourcePatternResolver().getResources(
            scriptsPath
        ).sortedBy { resource ->
            resource.filename
        }.filter { resource ->
            resource.filename?.matches(Regex("^\\d+-.*")) ?: false
        }.forEach { resource ->
            logger.info("Executing DDL: ${resource.filename}")
            exec(resource)
        }

        runMigrations()
    }

    fun exec(resource: Resource) = transaction {
        val sql: String = resource.inputStream.bufferedReader().readText()
        sql.split(";").filter { line ->
            line.isNotBlank()
        }.forEach { line ->
            exec("$line;")
        }
    }

    private fun runMigrations() {
        migrations.sortedBy {
            it.order
        }.forEach { migration ->
            if (migration.scope.contains(Environment.currentEnv)) {
                logger.info("executing migration: ${migration.description}")
                migration.execute()
            }
        }
    }
}
