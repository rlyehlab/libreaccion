package net.libreaccion.support.persistence

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Transaction
import javax.inject.Inject

abstract class TransactionSupport {

    @Inject
    protected lateinit var db: Database

    fun<T> transaction(statement: Transaction.() -> T): T {
        return org.jetbrains.exposed.sql.transactions.transaction(db) {
            statement()
        }
    }
}
