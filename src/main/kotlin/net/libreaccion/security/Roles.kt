package net.libreaccion.security

import net.libreaccion.domain.accounts.model.Role

/** Contains all the supported user roles in the application.
 */
object Roles {
    val ADMIN = Role("ADMIN", listOf());
    val USER = Role("USER", listOf());
    val all: List<Role> = listOf(ADMIN, USER)
    fun filter(callback: (Role) -> Boolean): List<Role> = all.filter(callback)
}
