package net.libreaccion.security

import net.libreaccion.server.ServerConfig
import org.pac4j.core.client.Client
import org.pac4j.core.client.direct.AnonymousClient
import org.pac4j.http.client.direct.DirectBearerAuthClient
import org.pac4j.http.client.indirect.FormClient
import org.pac4j.javalin.JavalinHttpActionAdapter
import org.pac4j.jwt.config.signature.SecretSignatureConfiguration
import org.pac4j.jwt.credentials.authenticator.JwtAuthenticator
import org.springframework.context.support.beans

object SecurityBeans {
    fun beans() = beans {
        bean<BcryptPasswordService>()
        bean<AccountUserPassAuthenticator>()
        bean<JavalinHttpActionAdapter>()

        // Clients
        bean {
            val serverConfig: ServerConfig = ref()
            val securityConfig: SecurityConfig = ref()
            FormClient("${serverConfig.baseUrl.removeSuffix("/")}/${securityConfig.loginPath.removePrefix("/")}", ref())
        }
        bean {
            val securityConfig: SecurityConfig = ref()
            DirectBearerAuthClient(
                JwtAuthenticator(
                    SecretSignatureConfiguration(securityConfig.jwtSalt)
                )
            )
        }
        bean<AnonymousClient>()

        // Config factory
        bean {
            SecurityProvider(
                clients = provider<Client<*>>().toList(),
                actionAdapter = ref(),
                securityConfig = ref()
            )
        }
    }
}
