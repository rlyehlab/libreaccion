package net.libreaccion.security

import net.libreaccion.application.store.DocumentModelFactory
import net.libreaccion.domain.accounts.model.Account
import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.accounts.model.Role
import net.libreaccion.domain.accounts.persistence.AccountDAO
import net.libreaccion.domain.accounts.persistence.ProfileDAO
import org.pac4j.core.context.WebContext
import org.pac4j.core.credentials.UsernamePasswordCredentials
import org.pac4j.core.credentials.authenticator.Authenticator
import org.pac4j.core.exception.CredentialsException
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.profile.definition.CommonProfileDefinition

class AccountUserPassAuthenticator(
    private val accountDAO: AccountDAO,
    private val profileDAO: ProfileDAO,
    private val documentModelFactory: DocumentModelFactory,
    private val bcryptPasswordService: BcryptPasswordService
) : Authenticator<UsernamePasswordCredentials> {
    override fun validate(
        credentials: UsernamePasswordCredentials,
        context: WebContext
    ) {
        val account: Account = accountDAO.findByEmail(credentials.username)
            ?: throw CredentialsException("user not found")
        if (!bcryptPasswordService.match(credentials.password, account.password)) {
            throw CredentialsException("user not found")
        }

        val profile: Profile = profileDAO.findByAccount(account)
            ?: throw CredentialsException("user not found")
        val profileRoles: List<Role> = accountDAO.getRoles(account, account.organizations.first());
        credentials.userProfile = CommonProfile().apply {
            id = account.id.toString()
            roles = profileRoles.map(Role::name).toSet()
            permissions = profileRoles.flatMap { role ->
                role.permissions
            }.toSet()
            addAttributes(mapOf(
                CommonProfileDefinition.EMAIL to account.email,
                CommonProfileDefinition.DISPLAY_NAME to profile.displayName,
                CommonProfileDefinition.PICTURE_URL to profile.photo?.let { documentModelFactory.from(profile.photo) },
            ))
        }
    }
}
