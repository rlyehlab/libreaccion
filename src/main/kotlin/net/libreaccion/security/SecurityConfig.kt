package net.libreaccion.security

data class SecurityConfig(
    val jwtSalt: String,
    val loginPath: String
)
