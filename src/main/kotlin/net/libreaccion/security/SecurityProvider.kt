package net.libreaccion.security

import io.javalin.Javalin
import io.javalin.core.JavalinConfig
import io.javalin.http.Context
import io.javalin.http.util.RateLimit
import org.pac4j.core.authorization.authorizer.RequireAnyRoleAuthorizer
import org.pac4j.core.client.Client
import org.pac4j.core.client.Clients
import org.pac4j.core.config.Config
import org.pac4j.core.config.ConfigFactory
import org.pac4j.core.http.adapter.HttpActionAdapter
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.profile.ProfileManager
import org.pac4j.core.profile.UserProfile
import org.pac4j.javalin.CallbackHandler
import org.pac4j.javalin.JavalinWebContext
import org.pac4j.javalin.SecurityHandler
import org.pac4j.jwt.config.signature.SecretSignatureConfiguration
import org.pac4j.jwt.profile.JwtGenerator
import java.util.*
import java.util.concurrent.TimeUnit

/** Default security config factory, it requires authenticated users to have any of the supported roles.
 */
class SecurityProvider(
    private val clients: List<Client<*>>,
    private val actionAdapter: HttpActionAdapter<*, *>,
    private val securityConfig: SecurityConfig
) : ConfigFactory {
    companion object {
        private const val JWT_TOKEN_ATTR = "jwtToken";
    }

    private val config: Config = build()

    override fun build(vararg parameters: Any): Config {
        val clients = Clients(securityConfig.loginPath, *clients.toTypedArray())
        return Config(clients).apply {
            addAuthorizer("default", RequireAnyRoleAuthorizer<UserProfile>(
                Roles.all.map { role -> role.name }
            ))
            httpActionAdapter = actionAdapter
        }
    }

    fun configure(appConfig: JavalinConfig) {
        appConfig.apply {
            enableCorsForAllOrigins()
        }
    }

    fun initialize(app: Javalin) {
        app.post(securityConfig.loginPath, loginCallback(CallbackHandler(config, null, true)))
        app.before(::generateJwtToken)
        app.after(::overrideServerHeader)
    }

    fun jwt(): SecurityHandler {
        return SecurityHandler(config, "ParameterClient,DirectBearerAuthClient")
    }

    fun form(): SecurityHandler {
        return SecurityHandler(config, "FormClient")
    }

    fun renderIndex(ctx: Context) {
        ctx.render("index.html", mapOf(
            JWT_TOKEN_ATTR to ctx.sessionAttribute(JWT_TOKEN_ATTR)
        ))
    }

    private fun loginCallback(callback: CallbackHandler): (Context) -> Unit {
        return { ctx ->
            RateLimit(ctx).requestPerTimeUnit(10, TimeUnit.MINUTES)
            callback.handle(ctx)
        }
    }

    private fun generateJwtToken(ctx: Context) {
        val context = JavalinWebContext(ctx)
        val manager = ProfileManager<CommonProfile>(context)

        if (manager.isAuthenticated) {
            val profile: Optional<CommonProfile> = manager.get(true)
            val token: String = JwtGenerator<CommonProfile>(
                SecretSignatureConfiguration(securityConfig.jwtSalt)
            ).generate(profile.get())
            ctx.sessionAttribute(JWT_TOKEN_ATTR, token)
        }
    }

    private fun overrideServerHeader(ctx: Context) {
        ctx.header("Server", "LibreAccion")
    }
}
