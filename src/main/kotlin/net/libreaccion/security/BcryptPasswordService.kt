package net.libreaccion.security

import at.favre.lib.crypto.bcrypt.BCrypt

class BcryptPasswordService(
    private val logRounds: Int = 10
) {

    companion object {
        private const val SIGNATURE: String = "{bcrypt}"
    }

    private val encoder: BCrypt.Hasher = BCrypt.withDefaults()
    private val verifier: BCrypt.Verifyer = BCrypt.verifyer()

    fun hash(plaintextPassword: Any): String {
        val hashedPassword: String = when (plaintextPassword) {
            is CharArray -> encoder.hashToString(logRounds, plaintextPassword)
            is String -> encoder.hashToString(logRounds, plaintextPassword.toCharArray())
            else -> throw IllegalArgumentException("unsupported password type")
        }
        return "$SIGNATURE$hashedPassword"
    }

    fun match(
        plaintext: Any,
        savedPassword: String
    ): Boolean {
        return if (!savedPassword.startsWith(SIGNATURE)) {
            false
        } else {
            val hash = savedPassword.substringAfter(SIGNATURE)

            when (plaintext) {
                is CharArray -> verifier.verify(plaintext, hash)
                is ByteArray -> verifier.verify(plaintext, hash.toByteArray())
                is String -> verifier.verify(plaintext.toCharArray(), hash)
                else -> throw IllegalArgumentException("unsupported password type")
            }.verified
        }
    }
}
