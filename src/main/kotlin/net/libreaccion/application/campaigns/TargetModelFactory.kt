package net.libreaccion.application.campaigns

import net.libreaccion.application.accounts.model.TargetDTO
import net.libreaccion.application.store.DocumentModelFactory
import net.libreaccion.application.store.model.DocumentDTO
import net.libreaccion.domain.campaigns.model.Target

class TargetModelFactory(
    private val documentModelFactory: DocumentModelFactory
) {
    fun from(target: Target): TargetDTO {
        val photo: DocumentDTO? = target.photo?.let(documentModelFactory::from)
        return TargetDTO.from(target, photo)
    }
}
