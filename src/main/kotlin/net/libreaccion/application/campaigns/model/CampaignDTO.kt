package net.libreaccion.application.campaigns.model

import net.libreaccion.application.accounts.model.ProfileDTO
import net.libreaccion.application.accounts.model.TargetDTO
import net.libreaccion.application.store.model.DocumentDTO
import net.libreaccion.domain.campaigns.model.Campaign
import org.joda.time.DateTime

data class CampaignDTO(
    val id: String,
    val coordinator: ProfileDTO,
    val organization: OrganizationDTO?,
    val friendlyId: String,
    val title: String,
    val description: String,
    val targets: List<TargetDTO>,
    val media: DocumentDTO?,
    val updatedAt: DateTime
) {
    companion object {
        fun from(
            campaign: Campaign,
            coordinator: ProfileDTO,
            targets: List<TargetDTO>,
            media: DocumentDTO?
        ): CampaignDTO = CampaignDTO(
            id = campaign.id,
            coordinator = coordinator,
            organization = campaign.organization?.let(OrganizationDTO::from),
            friendlyId = campaign.friendlyId,
            title = campaign.title,
            description = campaign.description,
            targets = targets,
            media = media,
            updatedAt = campaign.updatedAt
        )
    }
}
