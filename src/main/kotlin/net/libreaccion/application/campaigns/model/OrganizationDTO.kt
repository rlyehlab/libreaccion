package net.libreaccion.application.campaigns.model

import net.libreaccion.domain.accounts.model.Organization

data class OrganizationDTO(
    val id: String,
    val name: String,
    val contactEmail: String
) {
    companion object {
        fun from(organization: Organization): OrganizationDTO = OrganizationDTO(
            id = organization.id,
            name = organization.name,
            contactEmail = organization.contactEmail
        )
    }
}
