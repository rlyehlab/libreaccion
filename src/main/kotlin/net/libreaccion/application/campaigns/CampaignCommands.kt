package net.libreaccion.application.campaigns

import io.javalin.http.Context
import io.javalin.http.NotFoundResponse
import net.libreaccion.domain.campaigns.CampaignService
import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.support.pagination.PagingCommand
import org.jetbrains.exposed.dao.exceptions.EntityNotFoundException

class CampaignCommands(
    private val campaignService: CampaignService,
    private val campaignModelFactory: CampaignModelFactory
): PagingCommand() {
    fun getById(ctx: Context) {
        try {
            val campaign: Campaign = campaignService.get(ctx.pathParam("id"))
            ctx.json(campaignModelFactory.from(campaign))
        } catch (cause: EntityNotFoundException) {
            throw NotFoundResponse();
        }
    }

    fun list(ctx: Context) = paging(ctx) { cursor ->
        campaignService.list(cursor).map(campaignModelFactory::from)
    }
}
