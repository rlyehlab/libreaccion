package net.libreaccion.application.campaigns

import net.libreaccion.application.accounts.ProfileModelFactory
import net.libreaccion.application.accounts.model.ProfileDTO
import net.libreaccion.application.accounts.model.TargetDTO
import net.libreaccion.application.campaigns.model.CampaignDTO
import net.libreaccion.application.store.DocumentModelFactory
import net.libreaccion.application.store.model.DocumentDTO
import net.libreaccion.domain.campaigns.model.Campaign

class CampaignModelFactory(
    private val profileModelFactory: ProfileModelFactory,
    private val targetModelFactory: TargetModelFactory,
    private val documentModelFactory: DocumentModelFactory
) {
    fun from(campaign: Campaign): CampaignDTO {
        val coordinator: ProfileDTO = profileModelFactory.from(campaign.coordinator)
        val targets: List<TargetDTO> = campaign.targets.map(targetModelFactory::from)
        val media: DocumentDTO? = campaign.media?.let(documentModelFactory::from)
        return CampaignDTO.from(campaign, coordinator, targets, media)
    }
}