package net.libreaccion.application.store

import net.libreaccion.application.store.model.DocumentDTO
import net.libreaccion.domain.store.model.Document
import java.net.URI

class DocumentModelFactory(
    serverBaseUrl: String
) {
    private val baseUrl: String = "${serverBaseUrl.removeSuffix("/")}/content"

    fun from(document: Document): DocumentDTO {
        val url: String = URI.create("$baseUrl/${document.id}").toString()
        return DocumentDTO.from(document, url)
    }
}
