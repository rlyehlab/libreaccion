package net.libreaccion.application.store.model

import net.libreaccion.domain.store.model.Document

data class DocumentDTO(
    val id: String,
    val name: String,
    val type: String,
    val size: Long,
    val url: String
) {
    companion object {
        fun from(
            document: Document,
            url: String
        ): DocumentDTO = DocumentDTO(
            id = document.id,
            name = document.name,
            type = document.type,
            size = document.size,
            url = url
        )
    }
}
