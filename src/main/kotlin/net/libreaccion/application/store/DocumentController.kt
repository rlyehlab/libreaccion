package net.libreaccion.application.store

import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.NotFoundResponse
import io.javalin.http.UploadedFile
import net.libreaccion.domain.store.DataStore
import net.libreaccion.domain.store.model.Document

class DocumentController(
    private val dataStore: DataStore,
    private val documentModelFactory: DocumentModelFactory
) {
    fun read(ctx: Context) {
        dataStore.findById(ctx.pathParam("id"))?.let { document ->
            ctx.contentType(document.type)
                .result(dataStore.read(document))
        } ?: throw NotFoundResponse("Document not found")
    }

    fun getMetadata(ctx: Context) {
        dataStore.findById(ctx.pathParam("id"))?.let { document ->
            ctx.json(documentModelFactory.from(document))
        } ?: throw NotFoundResponse("Document not found")
    }

    fun upload(ctx: Context) {
        val file: UploadedFile = ctx.uploadedFile("file")
            ?: throw BadRequestResponse("file not found")
        val document: Document = Document.new(
            name = file.filename,
            type = ctx.formParam("type") ?: throw BadRequestResponse("invalid file type"),
            size = ctx.formParam("size")?.toLong() ?: throw BadRequestResponse("invalid file type"),
            meta = emptyMap()
        )
        ctx.json(documentModelFactory.from(
            dataStore.save(document, file.content)
        ))
    }
}
