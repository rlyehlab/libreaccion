package net.libreaccion.application

import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.core.JavalinConfig
import io.javalin.http.Context
import io.javalin.http.Handler
import io.javalin.plugin.json.JavalinJackson
import net.libreaccion.application.accounts.Sessions
import net.libreaccion.application.campaigns.CampaignCommands
import net.libreaccion.application.endorsements.EndorsementCommands
import net.libreaccion.application.store.DocumentController
import net.libreaccion.security.SecurityProvider
import net.libreaccion.support.ObjectMapperFactory
import net.libreaccion.support.persistence.TransactionSupport
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Endpoints(
    private val sessions: Sessions,
    private val campaignCommands: CampaignCommands,
    private val endorsementCommands: EndorsementCommands,
    private val documentController: DocumentController,
    private val securityProvider: SecurityProvider
): TransactionSupport() {
    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    init {
        JavalinJackson.configure(ObjectMapperFactory.camelCaseMapper)
    }

    fun configure(config: JavalinConfig) {
        config.apply {
            addSinglePageHandler("/", securityProvider::renderIndex)
            addStaticFiles("/frontend")
        }
    }

    fun register(app: Javalin) {
        app.routes {
            // Security
            before("/api/campaigns/new", securityProvider.jwt())
            before("/session/attributes", securityProvider.jwt())
            before("/content", securityProvider.jwt())
            before("/content/:id/meta", securityProvider.jwt())
            before("/campaigns/new", securityProvider.form())

            // Controllers
            get("/", runInTransaction(securityProvider::renderIndex))
            get("/api/campaigns/:id", runInTransaction(campaignCommands::getById))
            get("/api/campaigns/:id/endorsements", runInTransaction(endorsementCommands::listPublic))
            post("/api/campaigns/:id/endorsements", runInTransaction(endorsementCommands::endorse))
            get("/api/campaigns", runInTransaction(campaignCommands::list))

            get("/session", runInTransaction(sessions::currentSession))
            patch("/session/attributes", runInTransaction(sessions::setAttributes))
            get("/content/:id", runInTransaction(documentController::read))
            get("/content/:id/meta", runInTransaction(documentController::getMetadata))
            post("/content", runInTransaction(documentController::upload))
        }.exception(Exception::class.java) { cause, _ ->
            logger.error("Endpoint error", cause)
        }
    }

    private fun runInTransaction(handler: Handler): (Context) -> Unit {
        return { ctx: Context ->
            transaction {
                handler.handle(ctx)
            }
        }
    }
}
