package net.libreaccion.application.endorsements

import io.javalin.http.Context
import net.libreaccion.application.accounts.currentProfile
import net.libreaccion.domain.campaigns.CampaignService
import net.libreaccion.domain.endorsements.EndorsementService
import net.libreaccion.domain.endorsements.model.ActionType
import net.libreaccion.domain.endorsements.model.Endorsement
import net.libreaccion.support.pagination.PagingCommand

class EndorsementCommands(
    private val endorsementService: EndorsementService,
    private val endorsementModelFactory: EndorsementModelFactory,
    private val campaignService: CampaignService
) : PagingCommand() {
    fun endorse(ctx: Context) {
        val endorsement: Endorsement = endorsementService.endorse(
            profile = ctx.currentProfile(),
            campaign = campaignService.get(ctx.pathParam("id")),
            type = ActionType.valueOf(ctx.formParam<String>("type").get()),
            public = ctx.formParam<Boolean>("public").get()
        )
        ctx.json(endorsementModelFactory.public(endorsement))
    }

    fun listPublic(ctx: Context) = paging(ctx) { cursor ->
        endorsementService.listPublic(
            campaign = campaignService.get(ctx.pathParam("id")),
            cursor = cursor
        ).map(endorsementModelFactory::public)
    }
}
