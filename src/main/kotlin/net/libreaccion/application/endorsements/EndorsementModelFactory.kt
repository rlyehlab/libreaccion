package net.libreaccion.application.endorsements

import net.libreaccion.application.accounts.ProfileModelFactory
import net.libreaccion.application.accounts.model.ProfileDTO
import net.libreaccion.application.endorsements.model.PublicEndorsementDTO
import net.libreaccion.domain.endorsements.model.Endorsement

class EndorsementModelFactory(
    private val profileModelFactory: ProfileModelFactory
) {
    fun public(endorsement: Endorsement): PublicEndorsementDTO {
        val profile: ProfileDTO = profileModelFactory.from(endorsement.profile)

        return PublicEndorsementDTO.new(
            id = endorsement.id,
            profile = profile,
            campaignId = endorsement.campaign.id,
            type = endorsement.type.toString()
        )
    }
}
