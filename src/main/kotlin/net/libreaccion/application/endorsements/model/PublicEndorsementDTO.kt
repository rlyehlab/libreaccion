package net.libreaccion.application.endorsements.model

import net.libreaccion.application.accounts.model.ProfileDTO
import org.joda.time.DateTime

/** Represents an individual endorsement to a campaign.
 */
data class PublicEndorsementDTO(
    val id: String,
    val profile: ProfileDTO,
    val campaignId: String,
    val type: String
) {
    companion object {
        fun new(
            id: String,
            profile: ProfileDTO,
            campaignId: String,
            type: String
        ): PublicEndorsementDTO = PublicEndorsementDTO(
            id = id,
            profile = profile,
            campaignId = campaignId,
            type = type
        )
    }
}
