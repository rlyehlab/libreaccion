package net.libreaccion.application

import net.libreaccion.application.accounts.ProfileModelFactory
import net.libreaccion.application.accounts.SessionModelFactory
import net.libreaccion.application.accounts.Sessions
import net.libreaccion.application.campaigns.CampaignModelFactory
import net.libreaccion.application.campaigns.CampaignCommands
import net.libreaccion.application.campaigns.TargetModelFactory
import net.libreaccion.application.endorsements.EndorsementCommands
import net.libreaccion.application.endorsements.EndorsementModelFactory
import net.libreaccion.application.store.DocumentController
import net.libreaccion.application.store.DocumentModelFactory
import net.libreaccion.server.ServerConfig
import net.libreaccion.support.pagination.PageResultFactory
import org.springframework.context.support.beans
import org.thymeleaf.TemplateEngine
import org.thymeleaf.templatemode.TemplateMode
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver

object ApplicationBeans {
    fun beans() = beans {
        bean<PageResultFactory>()

        // Endpoints
        bean<CampaignCommands>()
        bean<Sessions>()
        bean<EndorsementCommands>()
        bean<DocumentController>()
        bean<Endpoints>()

        // Model factories
        bean<CampaignModelFactory>()
        bean<TargetModelFactory>()
        bean<ProfileModelFactory>()
        bean<SessionModelFactory>()
        bean<EndorsementModelFactory>()
        bean {
            val serverConfig: ServerConfig = ref()
            DocumentModelFactory(
                serverBaseUrl = serverConfig.baseUrl
            )
        }

        // Thymeleaf configuration beans.
        bean {
            ClassLoaderTemplateResolver().apply {
                prefix = "frontend/"
                suffix = ".html"
                templateMode = TemplateMode.HTML
                isCacheable = false
            }
        }
        bean {
            TemplateEngine().apply {
                setTemplateResolver(ref())
            }
        }
    }
}
