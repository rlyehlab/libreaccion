package net.libreaccion.application.accounts

import net.libreaccion.application.accounts.model.SessionDTO
import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.sessions.model.Session
import org.joda.time.DateTime

class SessionModelFactory(
    private val profileModelFactory: ProfileModelFactory
) {
    fun authenticated(
        profile: Profile,
        session: Session
    ): SessionDTO {
        return SessionDTO(
            profile = profileModelFactory.from(profile),
            expiration = DateTime.now(),
            authenticated = true,
            attributes = session.userData
        )
    }

    fun notAuthenticated() = SessionDTO(
        profile = null,
        expiration = DateTime.now(),
        authenticated = false,
        attributes = emptyMap()
    )
}