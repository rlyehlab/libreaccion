package net.libreaccion.application.accounts

import io.javalin.http.Context
import io.javalin.http.NotFoundResponse
import io.javalin.http.UnauthorizedResponse
import net.libreaccion.Application
import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.accounts.persistence.ProfileDAO
import net.libreaccion.domain.sessions.model.Session
import net.libreaccion.domain.sessions.persistence.SessionDAO
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.profile.ProfileManager
import org.pac4j.javalin.JavalinWebContext
import org.springframework.beans.factory.getBean

fun Context.isAuthenticated(): Boolean {
    val context = JavalinWebContext(this)
    val manager = ProfileManager<CommonProfile>(context)

    return manager.isAuthenticated
}

fun Context.currentProfile(): Profile {
    return if (isAuthenticated()) {
        val profileDAO: ProfileDAO = Application.context.getBean()
        val context = JavalinWebContext(this)
        val manager = ProfileManager<CommonProfile>(context)
        val commonProfile = manager.get(true).get()

        profileDAO.findByAccount(commonProfile.id)
            ?: throw NotFoundResponse("user not found")
    } else {
        throw UnauthorizedResponse("not authenticated")
    }
}

fun Context.currentSession(): Session {
    return if (isAuthenticated()) {
        val sessionDAO: SessionDAO = Application.context.getBean()
        sessionDAO.get(req.session.id)
    } else {
        throw UnauthorizedResponse("not authenticated")
    }
}
