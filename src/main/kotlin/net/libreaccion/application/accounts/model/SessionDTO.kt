package net.libreaccion.application.accounts.model

import org.joda.time.DateTime

/** Represents an active HTTP session.
 */
data class SessionDTO(
    val profile: ProfileDTO?,
    val expiration: DateTime,
    val authenticated: Boolean,
    val attributes: Map<String, Any>
)
