package net.libreaccion.application.accounts.model

import net.libreaccion.application.campaigns.model.OrganizationDTO
import net.libreaccion.application.store.model.DocumentDTO
import net.libreaccion.domain.accounts.model.Profile
import org.joda.time.DateTime

/** Represents a person or organization profile in the platform.
 */
data class ProfileDTO(
    val id: String,
    val organizations: List<OrganizationDTO>,
    val email: String,
    val userName: String,
    val displayName: String,
    val photo: DocumentDTO?,
    val updatedAt: DateTime
) {
    companion object {
        fun from(
            profile: Profile,
            photo: DocumentDTO?
        ): ProfileDTO {
            return ProfileDTO(
                id = profile.id,
                organizations = profile.account.organizations.map(OrganizationDTO::from),
                email = profile.account.email,
                userName = profile.userName,
                displayName = profile.displayName,
                photo = photo,
                updatedAt = profile.updatedAt
            )
        }
    }
}
