package net.libreaccion.application.accounts.model

import net.libreaccion.application.store.model.DocumentDTO
import net.libreaccion.domain.campaigns.model.Target
import org.joda.time.DateTime

/** Represents a campaign target person or organization.
 * It might have a profile in the platform.
 */
data class TargetDTO(
    val id: String,
    val displayName: String,
    val description: String,
    val photo: DocumentDTO?,
    val contactEmail: String?,
    val facebookAccount: String?,
    val twitterAccount: String?,
    val updatedAt: DateTime
) {
    companion object {
        fun from(
            target: Target,
            photo: DocumentDTO?
        ): TargetDTO = TargetDTO(
            id = target.id,
            displayName = target.displayName,
            description = target.description,
            photo = photo,
            contactEmail = target.contactEmail,
            facebookAccount = target.facebookAccount,
            twitterAccount = target.twitterAccount,
            updatedAt = target.updatedAt
        )
    }
}
