package net.libreaccion.application.accounts

import io.javalin.http.Context
import net.libreaccion.domain.sessions.persistence.SessionDAO
import net.libreaccion.support.persistence.TransactionSupport

/** Returns the current session information.
 */
class Sessions(
    private val sessionModelFactory: SessionModelFactory,
    private val sessionDAO: SessionDAO
): TransactionSupport() {
    fun currentSession(ctx: Context) {
        if (ctx.isAuthenticated()) {
            ctx.json(sessionModelFactory.authenticated(ctx.currentProfile(), ctx.currentSession()))
        } else {
            ctx.json(sessionModelFactory.notAuthenticated())
        }
    }

    fun setAttributes(ctx: Context) = transaction {
        val userData: Map<String, Any> = ctx.body<Map<String, Any>>()
        sessionDAO.saveOrUpdate(ctx.currentSession().addUserData(userData))
    }
}
