package net.libreaccion.application.accounts

import net.libreaccion.application.accounts.model.ProfileDTO
import net.libreaccion.application.store.DocumentModelFactory
import net.libreaccion.application.store.model.DocumentDTO
import net.libreaccion.domain.accounts.model.Profile

class ProfileModelFactory(
    private val documentModelFactory: DocumentModelFactory
) {
    fun from(profile: Profile): ProfileDTO {
        val photo: DocumentDTO? = profile.photo?.let(documentModelFactory::from)
        return ProfileDTO.from(profile, photo)
    }
}
