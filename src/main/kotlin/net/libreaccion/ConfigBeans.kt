package net.libreaccion

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import net.libreaccion.datasource.DataSourceConfig
import net.libreaccion.security.SecurityConfig
import net.libreaccion.server.ServerConfig
import org.springframework.context.support.beans

object ConfigBeans {
    fun beans() = beans {
        val mainConfig: Config = ConfigFactory.defaultApplication().resolve()

        bean {
            val dbConfig: Config = mainConfig.getConfig("db")

            DataSourceConfig(
                url = dbConfig.getString("url"),
                user = dbConfig.getString("user"),
                password = dbConfig.getString("password"),
                driver = dbConfig.getString("driver"),
                logStatements = dbConfig.getBoolean("log-statements"),
                drop = dbConfig.getString("drop")
            )
        }

        bean {
            val config: Config = mainConfig.getConfig("app.server")

            ServerConfig(
                host = config.getString("host"),
                port = config.getInt("port"),
                appName = config.getString("app-name"),
                contextPath = config.getString("context-path"),
                baseUrl = config.getString("base-url")
            )
        }

        bean {
            val config: Config = mainConfig.getConfig("app.security")

            SecurityConfig(
                jwtSalt = config.getString("jwt-salt"),
                loginPath = config.getString("login-path")
            )
        }
    }
}