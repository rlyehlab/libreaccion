package net.libreaccion.domain.campaigns.persistence

import net.libreaccion.domain.accounts.persistence.OrganizationEntity
import net.libreaccion.domain.accounts.persistence.Organizations
import net.libreaccion.domain.accounts.persistence.ProfileEntity
import net.libreaccion.domain.accounts.persistence.Profiles
import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.domain.store.persistence.DocumentEntity
import net.libreaccion.domain.store.persistence.Documents
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object Campaigns : IdTable<String>(name = "campaigns") {
    override val id = varchar("id", 190).entityId()
    val coordinator = reference("coordinator", Profiles)
    val organization = reference("organization", Organizations).nullable()
    val friendlyId = varchar("friendly_id", 255).uniqueIndex()
    val title = varchar("title", 255)
    val description = text("description")
    val media = reference("media", Documents).nullable()
    val createdAt = datetime("created_at")
    val updatedAt = datetime("updated_at")
    override val primaryKey = PrimaryKey(id)
}

object CampaignsTargets : Table(name = "campaigns_targets") {
    val campaign = reference("campaign", Campaigns)
    val target = reference("target", Targets)
}

class CampaignEntity(id: EntityID<String>) : AbstractEntity<Campaign>(id) {
    companion object : AbstractEntityClass<String, Campaign, CampaignEntity>(Campaigns)

    var coordinator: ProfileEntity by ProfileEntity referencedOn Campaigns.coordinator
    var organization: OrganizationEntity? by OrganizationEntity optionalReferencedOn Campaigns.organization
    var friendlyId: String by Campaigns.friendlyId
    var title: String by Campaigns.title
    var description: String by Campaigns.description
    var targets by TargetEntity via CampaignsTargets
    var media: DocumentEntity? by DocumentEntity optionalReferencedOn Campaigns.media
    var createdAt: DateTime by Campaigns.createdAt
    var updatedAt: DateTime by Campaigns.updatedAt

    override fun create(source: Campaign): AbstractEntity<Campaign> {
        coordinator = ProfileEntity[source.coordinator.id]
        organization = source.organization?.let {
            OrganizationEntity[source.organization.id]
        }
        createdAt = DateTime.now()
        return update(source)
    }

    override fun update(source: Campaign): CampaignEntity {
        friendlyId = source.friendlyId
        title = source.title
        description = source.description
        media = source.media?.let {
            DocumentEntity[source.media.id]
        }
        updatedAt = DateTime.now()
        targets = SizedCollection(source.targets.map { target -> TargetEntity[target.id] })
        return this
    }

    override fun toDomainType(): Campaign {
        return Campaign(
            id = id.value,
            coordinator = coordinator.toDomainType(),
            organization = organization?.toDomainType(),
            friendlyId = friendlyId,
            title = title,
            description = description,
            targets = targets.map { target ->
                target.toDomainType()
            },
            media = media?.toDomainType(),
            updatedAt = updatedAt
        )
    }
}
