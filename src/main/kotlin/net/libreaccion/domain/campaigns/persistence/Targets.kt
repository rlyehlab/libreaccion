package net.libreaccion.domain.campaigns.persistence

import net.libreaccion.domain.campaigns.model.Target
import net.libreaccion.domain.store.persistence.DocumentEntity
import net.libreaccion.domain.store.persistence.Documents
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object Targets : IdTable<String>(name = "targets") {
    override val id = varchar("id", 190).entityId()
    val displayName = varchar("display_name", 255)
    val description = varchar("description", 255)
    val photo = reference("photo", Documents).nullable()
    val contactEmail = varchar("contact_email", 255).nullable()
    val facebookAccount = text("facebook_account").nullable()
    val twitterAccount = text("twitter_account").nullable()
    val createdAt = datetime("created_at")
    val updatedAt = datetime("updated_at")
    override val primaryKey = PrimaryKey(id)
}

class TargetEntity(id: EntityID<String>) : AbstractEntity<Target>(id) {
    companion object : AbstractEntityClass<String, Target, TargetEntity>(Targets)

    var displayName: String by Targets.displayName
    var description: String by Targets.description
    var photo: DocumentEntity? by DocumentEntity optionalReferencedOn Targets.photo
    var contactEmail: String? by Targets.contactEmail
    var facebookAccount: String? by Targets.facebookAccount
    var twitterAccount: String? by Targets.twitterAccount
    var createdAt: DateTime by Targets.createdAt
    var updatedAt: DateTime by Targets.updatedAt

    override fun create(source: Target): AbstractEntity<Target> {
        createdAt = DateTime.now()
        return update(source)
    }

    override fun update(source: Target): TargetEntity {
        displayName = source.displayName
        description = source.description
        photo = source.photo?.let {
            DocumentEntity[source.photo.id.toString()]
        }
        contactEmail = source.contactEmail
        facebookAccount = source.facebookAccount
        twitterAccount = source.twitterAccount
        updatedAt = DateTime.now()
        return this
    }

    override fun toDomainType(): Target {
        return Target(
            id = id.value,
            displayName = displayName,
            description = description,
            photo = photo?.toDomainType(),
            contactEmail = contactEmail,
            facebookAccount = facebookAccount,
            twitterAccount = twitterAccount,
            updatedAt = updatedAt
        )
    }
}
