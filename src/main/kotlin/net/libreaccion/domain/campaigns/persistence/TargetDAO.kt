package net.libreaccion.domain.campaigns.persistence

import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.domain.campaigns.model.Target
import net.libreaccion.support.persistence.TransactionSupport

class TargetDAO : TransactionSupport() {
    fun saveOrUpdate(target: Target): Target = transaction {
        TargetEntity.saveOrUpdate(target.id, target)
    }

    /** Returns the target by id.
     * @param id Required target id.
     */
    fun get(id: String): Target = transaction {
        TargetEntity[id].toDomainType()
    }

    /** Returns the target by id.
     * @param id Required target id.
     */
    fun findById(id: String): Target? = transaction {
        TargetEntity.find {
            Targets.id eq id
        }.firstOrNull()?.toDomainType()
    }

    fun findByName(name: String): List<Target> = transaction {
        TargetEntity.find {
            Targets.displayName like name
        }.map(TargetEntity::toDomainType)
    }

    fun list(): List<Campaign> = transaction {
        CampaignEntity.all().map(CampaignEntity::toDomainType)
    }
}
