package net.libreaccion.domain.campaigns.persistence

import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.support.pagination.Cursor
import net.libreaccion.support.pagination.Page
import net.libreaccion.support.pagination.paging
import net.libreaccion.support.pagination.toPage
import net.libreaccion.support.persistence.TransactionSupport
import org.jetbrains.exposed.dao.exceptions.EntityNotFoundException
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.or

class CampaignDAO : TransactionSupport() {
    fun saveOrUpdate(campaign: Campaign): Campaign = transaction {
        CampaignEntity.saveOrUpdate(campaign.id, campaign)
    }

    /** Returns the campaign by id or friendly id.
     * @param id Either the entity id or the friendly campaign id.
     */
    fun get(id: String): Campaign = transaction {
        CampaignEntity.find {
            (Campaigns.id eq id) or
                (Campaigns.friendlyId eq id)
        }.firstOrNull()?.toDomainType()
            ?: throw EntityNotFoundException(EntityID(id, Campaigns), CampaignEntity)
    }

    fun list(cursor: Cursor): Page<Campaign> = transaction {
        CampaignEntity.all()
            .paging(cursor, Campaigns)
            .toPage(cursor)
    }
}
