package net.libreaccion.domain.campaigns.model

import net.libreaccion.domain.store.model.Document
import net.libreaccion.support.IdGenerator.randomId
import org.joda.time.DateTime

/** Represents a campaign target person or organization.
 * It might have a profile in the platform.
 */
data class Target(
    val id: String,
    val displayName: String,
    val description: String,
    val photo: Document?,
    val contactEmail: String?,
    val facebookAccount: String?,
    val twitterAccount: String?,
    val updatedAt: DateTime
) {
    companion object {
        fun new(
            displayName: String,
            description: String,
            photo: Document? = null,
            contactEmail: String? = null,
            facebookAccount: String? = null,
            twitterAccount: String? = null
        ): Target = Target(
            id = randomId(),
            displayName = displayName,
            description = description,
            photo = photo,
            contactEmail = contactEmail,
            facebookAccount = facebookAccount,
            twitterAccount = twitterAccount,
            updatedAt = DateTime.now()
        )
    }
}
