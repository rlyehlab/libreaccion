package net.libreaccion.domain.campaigns.model

import net.libreaccion.domain.accounts.model.Organization
import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.store.model.Document
import net.libreaccion.support.IdGenerator.randomId
import org.joda.time.DateTime

/** Represents a campaign.
 *
 * A campaign belongs to an organization. It has a coordinator, but it won't have an owner.
 *
 * If the coordinator leaves the organization, all campaigns will be transferred to someone
 * else within the organization.
 */
data class Campaign(
    val id: String,
    val friendlyId: String,
    val coordinator: Profile,
    val organization: Organization?,
    val title: String,
    val description: String,
    val targets: List<Target>,
    val media: Document?,
    val updatedAt: DateTime
) {
    companion object {
        fun new(
            coordinator: Profile,
            organization: Organization?,
            friendlyId: String,
            title: String,
            description: String,
            media: Document? = null
        ): Campaign = Campaign(
            id = randomId(),
            coordinator = coordinator,
            organization = organization,
            friendlyId = friendlyId,
            title = title,
            description = description,
            targets = emptyList(),
            media = media,
            updatedAt = DateTime.now()
        )
    }

    fun withTargets(newTargets: List<Target>): Campaign = copy(
        targets = newTargets
    )
}
