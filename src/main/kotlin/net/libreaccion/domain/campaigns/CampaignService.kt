package net.libreaccion.domain.campaigns

import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.domain.campaigns.model.Target
import net.libreaccion.domain.campaigns.persistence.CampaignDAO
import net.libreaccion.domain.campaigns.persistence.TargetDAO
import net.libreaccion.support.pagination.Cursor
import net.libreaccion.support.pagination.Page
import org.jetbrains.exposed.sql.transactions.transaction

class CampaignService(
    private val campaignDAO: CampaignDAO,
    private val targetDAO: TargetDAO
) {
    fun saveOrUpdate(campaign: Campaign): Campaign {
        return campaignDAO.saveOrUpdate(campaign)
    }

    /** Returns the campaign by id or friendly id.
     * @param id Either the entity id or the friendly campaign id.
     */
    fun get(id: String): Campaign {
        return campaignDAO.get(id)
    }

    fun list(cursor: Cursor): Page<Campaign> {
        return campaignDAO.list(cursor)
    }

    fun changeTargets(
        campaign: Campaign,
        targets: List<Target>
    ): Campaign = transaction {
        targets.forEach(targetDAO::saveOrUpdate)
        campaignDAO.saveOrUpdate(campaign.withTargets(targets))
    }
}
