package net.libreaccion.domain.sessions.persistence

import net.libreaccion.domain.sessions.model.Session
import net.libreaccion.support.persistence.TransactionSupport
import org.jetbrains.exposed.sql.deleteWhere

class SessionDAO : TransactionSupport() {
    fun get(id: String): Session = transaction {
        SessionEntity[id].toDomainType()
    }

    fun find(id: String): Session? = transaction {
        SessionEntity.find {
            Sessions.id eq id
        }.firstOrNull()?.toDomainType()
    }

    fun findAll(ids: Set<String>): Set<Session> = transaction {
        SessionEntity.find {
            Sessions.id inList (ids)
        }.map(SessionEntity::toDomainType).toSet()
    }

    fun delete(id: String): Boolean = transaction {
        try {
            Sessions.deleteWhere {
                Sessions.id eq id
            }
            true
        } catch (cause: Throwable) {
            false
        }
    }

    fun exists(id: String): Boolean = transaction {
        SessionEntity.find {
            Sessions.id eq id
        }.count() == 1L
    }

    fun saveOrUpdate(session: Session): Session = transaction {
        SessionEntity.saveOrUpdate(session.id, session)
    }
}
