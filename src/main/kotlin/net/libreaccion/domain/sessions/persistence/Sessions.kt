package net.libreaccion.domain.sessions.persistence

import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator
import com.fasterxml.jackson.module.kotlin.readValue
import net.libreaccion.domain.sessions.model.Session
import net.libreaccion.support.ObjectMapperFactory.snakeCaseMapper
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.eclipse.jetty.server.session.SessionData
import org.eclipse.jetty.util.ClassLoadingObjectInputStream
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream

object Sessions : IdTable<String>(name = "account_sessions") {
    override val id = varchar("id", 255).entityId()
    val contextPath = varchar("context_path", length = 255)
    val vhost = varchar("vhost", length = 255)
    val created = long("created")
    val accessed = long("accessed")
    val lastAccessed = long("last_accessed")
    val maxInactiveMs = long("max_inactive_ms")
    val attributes = binary("attributes", length = 0x80000)
    val userData = text("user_data")
    val expiration = datetime("expiration")
    override val primaryKey = PrimaryKey(id)
}

class SessionEntity(id: EntityID<String>) : AbstractEntity<Session>(id) {
    companion object : AbstractEntityClass<String, Session, SessionEntity>(Sessions)

    private var contextPath: String by Sessions.contextPath
    private var vhost: String by Sessions.vhost
    private var created: Long by Sessions.created
    private var accessed: Long by Sessions.accessed
    private var lastAccessed: Long by Sessions.lastAccessed
    private var maxInactiveMs: Long by Sessions.maxInactiveMs
    private var attributes: ByteArray by Sessions.attributes
    private var userData: String by Sessions.userData
    private var expiration: DateTime by Sessions.expiration

    private val objectMapper: ObjectMapper = snakeCaseMapper.copy().setDefaultTyping(
        DefaultTypeResolverBuilder(ObjectMapper.DefaultTyping.EVERYTHING, LaissezFaireSubTypeValidator())
            .init(JsonTypeInfo.Id.CLASS, null)
            .inclusion(JsonTypeInfo.As.PROPERTY)
            .typeProperty("@type")
    )

    override fun create(source: Session): AbstractEntity<Session> {
        contextPath = source.contextPath
        vhost = source.vhost
        created = source.created
        maxInactiveMs = source.maxInactiveMs
        expiration = source.expiration
        return update(source)
    }

    override fun update(source: Session): SessionEntity {
        accessed = source.accessed
        lastAccessed = source.lastAccessed
        val out = ByteArrayOutputStream()
        SessionData.serializeAttributes(source.toSessionData(), ObjectOutputStream(out))
        attributes = out.toByteArray()
        userData = objectMapper.writeValueAsString(source.userData)
        return this
    }

    override fun toDomainType(): Session {
        val session = Session.new(
            id = id.value,
            contextPath = contextPath,
            vhost = vhost,
            created = created,
            accessed = accessed,
            lastAccessed = lastAccessed,
            maxInactiveMs = maxInactiveMs,
            expiration = expiration
        )
        val input = ByteArrayInputStream(this@SessionEntity.attributes)
        val sessionData = session.toSessionData()
        SessionData.deserializeAttributes(sessionData, ClassLoadingObjectInputStream(input))

        return session.copy(
            attributes = sessionData.allAttributes,
            userData = objectMapper.readValue(this@SessionEntity.userData)
        )
    }
}
