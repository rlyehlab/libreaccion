package net.libreaccion.domain.sessions.model

import org.eclipse.jetty.server.session.SessionData
import org.joda.time.DateTime

/** Represents a document stored in the data store.
 */
data class Session(
    val id: String,
    val contextPath: String,
    val vhost: String,
    val created: Long,
    val accessed: Long,
    val lastAccessed: Long,
    val maxInactiveMs: Long,
    val attributes: Map<String, Any>,
    val userData: Map<String, Any>,
    val expiration: DateTime
) {
    companion object {
        fun new(
            id: String,
            contextPath: String,
            vhost: String,
            created: Long,
            accessed: Long,
            lastAccessed: Long,
            maxInactiveMs: Long,
            expiration: DateTime
        ): Session = Session(
            id = id,
            contextPath = contextPath,
            vhost = vhost,
            created = created,
            accessed = accessed,
            lastAccessed = lastAccessed,
            maxInactiveMs = maxInactiveMs,
            attributes = HashMap(),
            userData = HashMap(),
            expiration = expiration
        )
    }

    fun withAttributes(attributes: Map<String, Any>): Session = copy(
        attributes = attributes
    )

    fun addUserData(data: Map<String, Any>): Session = copy(
        userData = userData + data
    )

    fun toSessionData(): SessionData = SessionData(
        id,
        contextPath,
        vhost,
        created,
        accessed,
        lastAccessed,
        maxInactiveMs
    ).apply {
        putAllAttributes(attributes)
    }
}
