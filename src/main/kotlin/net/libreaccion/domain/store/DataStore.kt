package net.libreaccion.domain.store

import io.seruco.encoding.base62.Base62
import net.libreaccion.domain.store.HashUtils.hash
import net.libreaccion.domain.store.model.Document
import net.libreaccion.domain.store.persistence.DocumentDAO
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.InputStream

/** This class must be extended to implement a data store.
 *
 * Content is hashed with the xxHash algorithm and the hash is used as the document identifier. The
 * document content is saved into the backend store only once at the creation time. After that, it
 * will only update the document mutable state.
 *
 * Concurrency is managed by an optimistic lock in a database. The lock is set only during the creation time
 * to avoid data corruption. Updates will not lock the document. Concurrent updates will overwrite the document
 * state in the database.
 */
abstract class DataStore(
    private val documentDAO: DocumentDAO
) {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(DataStore::class.java)
    }

    /** Stores a document in the backend device, invoked only once during the document creation.
     * Any error will prevent the document from being created.
     *
     * @param document Document to store.
     * @param content Actual content to write into the store.
     * @return the updated document that will be saved into the database.
     */
    protected abstract fun store(
        document: Document,
        content: InputStream
    ): Document

    /** Retrieves the document content from the backend device.
     * It throws an error if the document doesn't exist.
     *
     * @param document Document to retrieve.
     * @return an input stream to read the document content.
     */
    abstract fun read(document: Document): InputStream

    /** Removes the document from the backend device.
     *
     * It doesn't fail if the file doesn't exist.
     *
     * @param document Document to delete.
     * @return the removed document.
     */
    protected abstract fun delete(document: Document): Document

    /** Stores the document in the data store.
     * If the document exists, it will only update the mutable state.
     * @param document Document to save.
     * @param content Content to write into the backend device.
     */
    fun save(
        document: Document,
        content: InputStream
    ): Document  = documentDAO.transaction {
        val resolvedDoc: Document = if (document.id.isBlank()) {
            storeDocument(document, content)
        } else {
            val existingDoc: Document? = documentDAO.findById(document.id)
            val mustStore: Boolean = existingDoc == null || existingDoc.deleted

            if (mustStore) {
                storeDocument(document, content)
            } else {
                document
            }
        }

        documentDAO.saveOrUpdate(resolvedDoc)
    }

    /** Updates the mutable state of a document.
     * @param document Document to update.
     * @return the updated document.
     */
    fun update(document: Document): Document {
        return documentDAO.saveOrUpdate(document)
    }

    /** Returns a document with the specified identifier.
     * It returns the document even if it's deleted.
     *
     * @param id Required document identifier.
     */
    fun get(id: String): Document {
        logger.info("getting document $id")
        return documentDAO.get(id)
    }

    /** Finds a document by identifier.
     * It doesn't return deleted documents.
     *
     * @param id Required document identifier.
     * @return the document, or null if it doesn't exist.
     */
    fun findById(id: String): Document? {
        logger.info("finding document $id")
        return documentDAO.findById(id)
    }

    /** Reads a document content.
     *
     * @param id Required document identifier.
     * @return an input stream to read the document content.
     */
    fun read(id: String): InputStream {
        logger.info("reading content for document $id")
        return read(get(id))
    }

    /** Deletes a document.
     * @param id Id of the document to delete.
     * @return the removed document.
     */
    fun delete(id: String): Document {
        logger.info("deleting document $id")
        val document: Document = delete(documentDAO.get(id))

        return documentDAO.saveOrUpdate(document.delete())
    }

    private fun createTempFile(input: InputStream): File {
        val tempFile: File = File.createTempFile("ds-", "-tmp")
        input.use {
            input.copyTo(tempFile.outputStream())
        }
        tempFile.deleteOnExit()

        logger.debug("temp file created: ${tempFile.absolutePath}")

        return tempFile
    }

    private fun storeDocument(
        document: Document,
        content: InputStream
    ): Document {
        val tempFile: File = createTempFile(content)
        val documentId: String = if (document.id.isBlank()) {
            val id = hash(tempFile.inputStream())
            logger.info("document $id doesn't yet exist, creating")
            id
        } else {
            logger.info("storing document ${document.id}")
            document.id
        }
        require(!documentDAO.isLocked(documentId)) { "document $documentId is locked" }

        documentDAO.lock(documentId)

        val newDocument: Document = store(document.copy(id = documentId), tempFile.inputStream())
        tempFile.delete()

        logger.info("document $documentId stored successfully, releasing lock")
        documentDAO.release(documentId)
        return newDocument
    }
}
