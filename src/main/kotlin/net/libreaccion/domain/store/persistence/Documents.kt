package net.libreaccion.domain.store.persistence

import com.fasterxml.jackson.module.kotlin.readValue
import net.libreaccion.domain.store.model.Document
import net.libreaccion.support.ObjectMapperFactory.snakeCaseMapper
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object Documents : IdTable<String>(name = "documents") {
    override val id = varchar("id", 27).entityId()
    val name = varchar("name", 255)
    val type = varchar("type", 255)
    val size = long("size")
    val meta = text("meta")
    val deleted = bool("is_deleted")
    override val primaryKey = PrimaryKey(id)
}

class DocumentEntity(id: EntityID<String>) : AbstractEntity<Document>(id) {
    companion object : AbstractEntityClass<String, Document, DocumentEntity>(Documents)

    var name: String by Documents.name
    var type: String by Documents.type
    var size: Long by Documents.size
    var meta: String by Documents.meta
    var deleted: Boolean by Documents.deleted

    override fun create(source: Document): AbstractEntity<Document> {
        size = source.size
        return update(source)
    }

    override fun update(source: Document): DocumentEntity {
        name = source.name
        type = source.type
        meta = snakeCaseMapper.writeValueAsString(source.meta)
        deleted = source.deleted
        return this
    }

    override fun toDomainType(): Document {
        return Document(
            id = id.value,
            deleted = deleted,
            name = name,
            type = type,
            size = size,
            meta = snakeCaseMapper.readValue(meta)
        )
    }
}
