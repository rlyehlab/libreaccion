package net.libreaccion.domain.store.persistence

import net.libreaccion.domain.store.model.Document
import net.libreaccion.domain.store.model.Lock
import net.libreaccion.support.persistence.TransactionSupport
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere

class DocumentDAO : TransactionSupport() {
    fun get(id: String): Document = transaction {
        DocumentEntity[id].toDomainType()
    }

    fun findById(id: String): Document? = transaction {
        DocumentEntity.find {
            (Documents.id eq id) and (Documents.deleted eq false)
        }.firstOrNull()?.toDomainType()
    }

    fun saveOrUpdate(document: Document): Document = transaction {
        DocumentEntity.saveOrUpdate(document.id, document)
    }

    fun isLocked(id: String): Boolean = transaction {
        LockEntity.findById(id)?.let {
            true
        } ?: false
    }

    fun lock(id: String) = transaction {
        LockEntity.saveOrUpdate(id, Lock.new(id))
    }

    fun release(id: String) = transaction {
        Locks.deleteWhere {
            Locks.id eq id
        }
    }
}
