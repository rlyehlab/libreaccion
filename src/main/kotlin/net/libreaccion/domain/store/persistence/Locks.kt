package net.libreaccion.domain.store.persistence

import net.libreaccion.domain.store.model.Lock
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object Locks : IdTable<String>(name = "document_locks") {
    override val id = varchar("id", 27).entityId()
    val createdAt = datetime("created_at")
    override val primaryKey = PrimaryKey(id)
}

class LockEntity(id: EntityID<String>) : AbstractEntity<Lock>(id) {
    companion object : AbstractEntityClass<String, Lock, LockEntity>(Locks)

    var createdAt: DateTime by Locks.createdAt

    override fun create(source: Lock): AbstractEntity<Lock> {
        createdAt = DateTime.now()
        return update(source)
    }

    override fun update(source: Lock): LockEntity {
        return this
    }

    override fun toDomainType(): Lock {
        return Lock(
            documentId = id.value,
            createdAt = createdAt
        )
    }
}
