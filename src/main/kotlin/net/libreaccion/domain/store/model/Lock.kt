package net.libreaccion.domain.store.model

import org.joda.time.DateTime

/** Represents a document write lock.
 *
 * Locks are temporary synchronization objects to avoid concurrency issues while a
 * [net.libreaccion.domain.store.DataStore] is writing a document.
 */
data class Lock(
    val documentId: String,
    val createdAt: DateTime
) {
    companion object {
        fun new(documentId: String): Lock = Lock(
            documentId = documentId,
            createdAt = DateTime.now()
        )
    }
}
