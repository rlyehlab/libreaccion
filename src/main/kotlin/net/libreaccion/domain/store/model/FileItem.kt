package net.libreaccion.domain.store.model

/** Represents a document stored in the [net.libreaccion.domain.store.FileSystemDataStore].
 */
data class FileItem(
    val document: Document,
    val content: String,
    val deleted: Boolean
) {
    companion object {
        fun new(
            document: Document,
            content: String
        ): FileItem = FileItem(
            document = document,
            content = content,
            deleted = false
        )
    }

    fun delete(): FileItem = copy(
        deleted = true
    )
}
