package net.libreaccion.domain.store.model

/** Represents a document stored in the data store.
 */
data class Document(
    val id: String,
    val name: String,
    val type: String,
    val size: Long,
    val meta: Map<String, String>,
    val deleted: Boolean
) {
    companion object {
        fun new(
            name: String,
            type: String,
            size: Long,
            meta: Map<String, String>
        ): Document = Document(
            id = "",
            name = name,
            type = type,
            size = size,
            meta = meta,
            deleted = false
        )
    }

    fun delete(): Document = copy(
        deleted = true
    )
}
