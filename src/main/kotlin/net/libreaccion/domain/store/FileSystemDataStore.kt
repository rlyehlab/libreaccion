package net.libreaccion.domain.store

import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import net.libreaccion.domain.store.model.Document
import net.libreaccion.domain.store.model.FileItem
import net.libreaccion.domain.store.persistence.DocumentDAO
import net.libreaccion.support.ObjectMapperFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.InputStream

/** Data store that writes documents to the file system.
 *
 * This data store keeps an internal index that's stored in JSON format in order to support quick access and
 * common file operations (move, rename, etc). Getting a document is an O(1) operation. The index is updated
 * asynchronously every time a _write_ operation occurs on a document. The index provides _eventual consistency_
 * on all _read_ operations.
 *
 * The delete operation performs a _logical delete_. The document is marked as deleted in the index. Trying to
 * access a deleted document will throw an error. This data store does not implement a garbage collector.
 * Garbage collection should be implemented outside this component.
 */
class FileSystemDataStore(
    documentDAO: DocumentDAO,
    /** Directory where the data store will write files. */
    private val directory: File
) : DataStore(documentDAO) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(FileSystemDataStore::class.java)
        private const val INDEX_FILE_NAME = "index.json"
    }

    private val indexFile = File(directory, INDEX_FILE_NAME)
    private val index: MutableMap<String, FileItem>
    private val indexWriteMutex: Mutex = Mutex()

    init {
        if (!directory.exists()) {
            logger.info("data store directory doesn't exist, creating $directory")
            directory.mkdirs()
        }
        index = if (indexFile.exists()) {
            logger.info("index file exists, reading into memory")
            ObjectMapperFactory.snakeCaseMapper.readValue(indexFile)
        } else {
            mutableMapOf()
        }
    }

    override fun store(
        document: Document,
        content: InputStream
    ): Document {
        // Document doesn't exist or it's deleted.
        val overwrite: Boolean = index[document.id]?.deleted ?: !index.contains(document.id)

        if (overwrite) {
            val path = resolveDocumentPath(document)
            content.use {
                content.transferTo(path.outputStream())
            }
            index[document.id] = FileItem.new(document, path.relativeTo(directory).toString())
            writeIndex()
        }

        return document
    }

    override fun read(document: Document): InputStream {
        val deleted: Boolean = index[document.id]?.deleted ?: false

        require(index.contains(document.id) && !deleted) {
            "The document ${document.id} does not exist"
        }

        return File(directory, index.getValue(document.id).content).inputStream()
    }

    override fun delete(document: Document): Document {
        if (index.contains(document.id)) {
            index[document.id] = index.getValue(document.id).delete()
            writeIndex()
        }
        return document
    }

    /** Resolves the path to write a document.
     *
     * In order to balance the directory tree, it uses the first three pair of digits from the document
     * id to build the path. E.g.: document 123456789 will be stored in `12/34/56/123456789`.
     *
     * This method ensures the directory exists.
     *
     * @return the document path.
     */
    internal fun resolveDocumentPath(document: Document): File {
        val id: String = document.id.toString()
        val path: File = directory.resolve(
            "${id.substring(0..2)}/${id.substring(2..4)}/${id.substring(4..6)}"
        )

        if (!path.exists()) {
            path.mkdirs()
        }

        return File(path, document.id)
    }

    /** Non-blocking operation to write the index to disk.
     * It uses a mutex to synchronize writes.
     */
    private fun writeIndex() = GlobalScope.launch(Dispatchers.IO) {
        indexWriteMutex.withLock {
            ObjectMapperFactory.snakeCaseMapper.writeValue(indexFile, index)
        }
    }
}
