package net.libreaccion.domain.store

import io.seruco.encoding.base62.Base62
import net.jpountz.xxhash.StreamingXXHash64
import net.jpountz.xxhash.XXHashFactory
import java.io.InputStream
import java.nio.charset.Charset

object HashUtils {
    private const val READ_BUFFER_SIZE: Int = 8192
    private const val HASH_SEED: Long = 0x1337
    private val base62: Base62 = Base62.createInstance()

    fun hash(input: InputStream): String {
        val factory: XXHashFactory = XXHashFactory.fastestInstance()
        val hash: StreamingXXHash64 = factory.newStreamingHash64(HASH_SEED)
        val buf = ByteArray(READ_BUFFER_SIZE)

        input.use {
            while (true) {
                val read: Int = input.read(buf)
                if (read == -1) {
                    break
                }
                hash.update(buf, 0, read)
            }
        }

        return base62.encode(hash.value.toString().toByteArray()).toString(Charset.defaultCharset())
    }
}
