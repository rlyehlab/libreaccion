package net.libreaccion.domain.accounts.model

data class Role(
    val name: String,
    val permissions: List<String>
) {
    companion object {
        fun new(
            name: String,
            permissions: List<String>
        ): Role = Role(
            name = name,
            permissions = permissions
        )
    }
}
