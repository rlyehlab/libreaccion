package net.libreaccion.domain.accounts.model

import net.libreaccion.support.IdGenerator.randomId

/** Represents a user account within anaccountOrganization organization.
 */
data class AccountOrganization(
    val id: String,
    val account: Account,
    val organization: Organization,
    val roles: List<String>
) {
    companion object {
        fun new(
            account: Account,
            organization: Organization,
            roles: List<Role>
        ): AccountOrganization {
            return AccountOrganization(
                id = randomId(),
                account = account,
                organization = organization,
                roles = roles.map(Role::name)
            )
        }
    }

    fun withRoles(roles: List<Role>): AccountOrganization = copy(
        roles = roles.map { role -> role.name }
    )
}
