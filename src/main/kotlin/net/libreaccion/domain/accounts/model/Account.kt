package net.libreaccion.domain.accounts.model

import net.libreaccion.support.IdGenerator.randomId
import org.joda.time.DateTime

/** Represents a user account.
 */
data class Account(
    val id: String,
    val organizations: List<Organization>,
    val email: String,
    val password: String,
    val enabled: Boolean,
    val updatedAt: DateTime
) {
    companion object {
        fun new(
            organizations: List<Organization>,
            email: String,
            password: String
        ): Account {
            return Account(
                id = randomId(),
                organizations = organizations,
                email = email,
                password = password,
                enabled = true,
                updatedAt = DateTime.now()
            )
        }
    }
}
