package net.libreaccion.domain.accounts.model

import net.libreaccion.domain.store.model.Document
import net.libreaccion.support.IdGenerator.randomId
import org.joda.time.DateTime

/** Represents a person or organization profile in the platform.
 */
data class Profile(
    val id: String,
    val account: Account,
    val userName: String,
    val displayName: String,
    val photo: Document?,
    val updatedAt: DateTime
) {
    companion object {
        fun new(
            account: Account,
            userName: String,
            displayName: String,
            photo: Document? = null
        ): Profile {
            return Profile(
                id = randomId(),
                account = account,
                userName = userName,
                displayName = displayName,
                photo = photo,
                updatedAt = DateTime.now()
            )
        }
    }
}
