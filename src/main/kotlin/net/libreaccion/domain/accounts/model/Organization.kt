package net.libreaccion.domain.accounts.model

import net.libreaccion.support.IdGenerator.randomId

/** Represents an organization within the platform.
 *
 * The organization can be associated to multiple accounts. Several permissions are
 * organization-based, like managing campaigns.
 */
data class Organization(
    val id: String,
    val name: String,
    val contactEmail: String,
    val public: Boolean
) {
    companion object {
        fun new(
            name: String,
            contactEmail: String,
            public: Boolean
        ): Organization = Organization(
            id = randomId(),
            name = name,
            contactEmail = contactEmail,
            public = public
        )
    }
}
