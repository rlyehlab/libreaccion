package net.libreaccion.domain.accounts.model

data class Permission(
    val name: String
) {
    companion object {
        fun new(name: String): Permission = Permission(
            name = name
        )
    }
}
