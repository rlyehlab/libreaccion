package net.libreaccion.domain.accounts.persistence

import net.libreaccion.domain.accounts.model.Account
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object Accounts : IdTable<String>(name = "accounts") {
    override val id = varchar("id", 190).entityId()
    val email = varchar("email", 255).uniqueIndex()
    val password = varchar("password", 255)
    val enabled = bool("enabled")
    val createdAt = datetime("created_at")
    val updatedAt = datetime("updated_at")
    override val primaryKey = PrimaryKey(id)
}

class AccountEntity(id: EntityID<String>) : AbstractEntity<Account>(id) {
    companion object : AbstractEntityClass<String, Account, AccountEntity>(Accounts)

    var organizations by OrganizationEntity via AccountOrganizations
    var email: String by Accounts.email
    var password: String by Accounts.password
    var enabled: Boolean by Accounts.enabled
    var createdAt: DateTime by Accounts.createdAt
    var updatedAt: DateTime by Accounts.updatedAt

    override fun create(source: Account): AbstractEntity<Account> {
        createdAt = DateTime.now()
        return update(source)
    }

    override fun update(source: Account): AccountEntity {
        email = source.email
        password = source.password
        enabled = source.enabled
        updatedAt = DateTime.now()
        return this
    }

    override fun toDomainType(): Account {
        return Account(
            id = id.value,
            enabled = enabled,
            organizations = organizations.map { accountOrganizationEntity ->
                accountOrganizationEntity.toDomainType()
            },
            email = email,
            password = password,
            updatedAt = updatedAt
        )
    }
}
