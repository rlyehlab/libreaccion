package net.libreaccion.domain.accounts.persistence

import net.libreaccion.domain.accounts.model.Organization
import net.libreaccion.support.persistence.TransactionSupport

class OrganizationDAO : TransactionSupport() {
    fun findByName(name: String): Organization? = transaction {
        OrganizationEntity.find {
            Organizations.name eq name
        }.firstOrNull()?.toDomainType()
    }

    fun saveOrUpdate(organization: Organization): Organization = transaction {
        OrganizationEntity.saveOrUpdate(organization.id, organization)
    }
}
