package net.libreaccion.domain.accounts.persistence

import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.store.persistence.DocumentEntity
import net.libreaccion.domain.store.persistence.Documents
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object Profiles : IdTable<String>(name = "profiles") {
    override val id = varchar("id", 190).entityId()
    val account = reference("account", Accounts)
    val userName = varchar("user_name", 50).uniqueIndex()
    val displayName = varchar("display_name", 255)
    val photo = reference("photo", Documents).nullable()
    val createdAt = datetime("created_at")
    val updatedAt = datetime("updated_at")
    override val primaryKey = PrimaryKey(id)
}

class ProfileEntity(id: EntityID<String>) : AbstractEntity<Profile>(id) {
    companion object : AbstractEntityClass<String, Profile, ProfileEntity>(Profiles)

    var account: AccountEntity by AccountEntity referencedOn Profiles.account
    var userName: String by Profiles.userName
    var displayName: String by Profiles.displayName
    var photo: DocumentEntity? by DocumentEntity optionalReferencedOn Profiles.photo
    var createdAt: DateTime by Profiles.createdAt
    var updatedAt: DateTime by Profiles.updatedAt

    override fun create(source: Profile): AbstractEntity<Profile> {
        account = AccountEntity[source.account.id]
        createdAt = DateTime.now()
        return update(source)
    }

    override fun update(source: Profile): ProfileEntity {
        userName = source.userName
        displayName = source.displayName
        photo = source.photo?.let {
            DocumentEntity[source.photo.id.toString()]
        }
        updatedAt = DateTime.now()
        return this
    }

    override fun toDomainType(): Profile {
        return Profile(
            id = id.value,
            account = account.toDomainType(),
            userName = userName,
            displayName = displayName,
            photo = photo?.toDomainType(),
            updatedAt = updatedAt
        )
    }
}
