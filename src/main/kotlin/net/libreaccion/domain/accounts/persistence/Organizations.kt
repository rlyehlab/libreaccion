package net.libreaccion.domain.accounts.persistence

import net.libreaccion.domain.accounts.model.Organization
import net.libreaccion.domain.store.persistence.Locks
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Table

object Organizations : IdTable<String>(name = "organizations") {
    override val id = varchar("id", 190).entityId()
    val name = varchar("name", 255)
    val contactEmail = varchar("contact_email", 255)
    val public = bool("public")
    override val primaryKey = PrimaryKey(id)
}

object OrganizationsRoles : Table(name = "organizations_roles") {
    val organization = reference("organization", Organizations)
    val profile = reference("profile", Profiles)
    val roles = varchar("roles", 255)
    override val primaryKey = PrimaryKey(Locks.id)
}

class OrganizationEntity(id: EntityID<String>) : AbstractEntity<Organization>(id) {
    companion object : AbstractEntityClass<String, Organization, OrganizationEntity>(Organizations)

    var name: String by Organizations.name
    var contactEmail: String by Organizations.contactEmail
    var public: Boolean by Organizations.public

    override fun create(source: Organization): AbstractEntity<Organization> {
        return update(source)
    }

    override fun update(source: Organization): OrganizationEntity {
        name = source.name
        contactEmail = source.contactEmail
        public = source.public
        return this
    }

    override fun toDomainType(): Organization {
        return Organization(
            id = id.value,
            name = name,
            contactEmail = contactEmail,
            public = public
        )
    }
}
