package net.libreaccion.domain.accounts.persistence

import net.libreaccion.domain.accounts.model.Account
import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.support.persistence.TransactionSupport
import org.jetbrains.exposed.dao.id.EntityID

class ProfileDAO : TransactionSupport() {
    fun findByAccount(account: Account): Profile? = transaction {
        ProfileEntity.find {
            Profiles.account eq EntityID(account.id.toString(), Accounts)
        }.firstOrNull()?.toDomainType()
    }

    fun findByAccount(accountId: String): Profile? = transaction {
        ProfileEntity.find {
            Profiles.account eq EntityID(accountId, Accounts)
        }.firstOrNull()?.toDomainType()
    }

    fun findByUser(userName: String): Profile? = transaction {
        ProfileEntity.find {
            Profiles.userName eq userName
        }.firstOrNull()?.toDomainType()
    }

    fun saveOrUpdate(profile: Profile): Profile = transaction {
        ProfileEntity.saveOrUpdate(profile.id, profile)
    }
}
