package net.libreaccion.domain.accounts.persistence

import net.libreaccion.domain.accounts.model.AccountOrganization
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object AccountOrganizations : IdTable<String>(name = "accounts_organizations") {
    override val id = varchar("id", 190).entityId()
    val account = reference("account", Accounts)
    val organization = reference("organization", Organizations)
    val roles = varchar("roles", 255)
    override val primaryKey = PrimaryKey(account, organization, name = "PK_Accounts_Organizations")
}

class AccountOrganizationEntity(id: EntityID<String>) : AbstractEntity<AccountOrganization>(id) {
    companion object : AbstractEntityClass<String, AccountOrganization, AccountOrganizationEntity>(AccountOrganizations)

    var account by AccountEntity referencedOn AccountOrganizations.account
    var organization by OrganizationEntity referencedOn AccountOrganizations.organization
    var roles: String by AccountOrganizations.roles

    override fun create(source: AccountOrganization): AbstractEntity<AccountOrganization> {
        account = AccountEntity[source.account.id]
        organization = OrganizationEntity[source.organization.id]
        return update(source)
    }

    override fun update(source: AccountOrganization): AccountOrganizationEntity {
        roles = source.roles.joinToString(",")
        return this
    }

    override fun toDomainType(): AccountOrganization {
        return AccountOrganization(
            id = id.value,
            account = account.toDomainType(),
            organization = organization.toDomainType(),
            roles = roles.split(",")
        )
    }
}
