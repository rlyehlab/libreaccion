package net.libreaccion.domain.accounts.persistence

import net.libreaccion.domain.accounts.model.*
import net.libreaccion.security.Roles
import net.libreaccion.support.persistence.TransactionSupport
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.and

class AccountDAO : TransactionSupport() {
    fun findByEmail(email: String): Account? = transaction {
        AccountEntity.find {
            Accounts.email eq email
        }.firstOrNull()?.toDomainType()
    }

    fun assignRoles(
        account: Account,
        organization: Organization,
        newRoles: List<Role>
    ): Account = transaction {
        val accountOrganization: AccountOrganization = AccountOrganizationEntity.find {
            (AccountOrganizations.account eq EntityID(account.id, AccountOrganizations)) and
            (AccountOrganizations.organization eq EntityID(organization.id, AccountOrganizations))
        }.firstOrNull()?.toDomainType() ?: let {
            val accountOrganization = AccountOrganization.new(account, organization, newRoles)
            AccountOrganizationEntity.saveOrUpdate(accountOrganization.id, accountOrganization)
        }

        AccountOrganizationEntity.saveOrUpdate(accountOrganization.id, accountOrganization.withRoles(newRoles))
        account
    }

    fun getRoles(
        account: Account,
        organization: Organization
    ): List<Role> = transaction {
        val roleNames = AccountOrganizationEntity.find {
            AccountOrganizations.account eq EntityID(account.id, Accounts)
            AccountOrganizations.organization eq EntityID(organization.id, Organizations)
        }.firstOrNull()?.roles?.split(",") ?: emptyList()

        val resolvedRoles = Roles.filter { role ->
            roleNames.contains(role.name)
        }

        assert(resolvedRoles.size == roleNames.size) {
            "Missing roles: ${roleNames.joinToString(",")}"
        }
        resolvedRoles
    }

    fun saveOrUpdate(account: Account): Account = transaction {
        AccountEntity.saveOrUpdate(account.id, account)
    }
}
