package net.libreaccion.domain.endorsements.persistence

import net.libreaccion.domain.accounts.persistence.ProfileEntity
import net.libreaccion.domain.accounts.persistence.Profiles
import net.libreaccion.domain.campaigns.persistence.CampaignEntity
import net.libreaccion.domain.campaigns.persistence.Campaigns
import net.libreaccion.domain.endorsements.model.ActionType
import net.libreaccion.domain.endorsements.model.Endorsement
import net.libreaccion.support.persistence.AbstractEntity
import net.libreaccion.support.persistence.AbstractEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object Endorsements : IdTable<String>(name = "endorsements") {
    override val id = varchar("id", 190).entityId()
    val profile = reference("profile", Profiles)
    val campaign = reference("campaign", Campaigns)
    val type = enumeration("action_type", ActionType::class)
    val public = bool("is_public")
    val createdAt = datetime("created_at")
    override val primaryKey = PrimaryKey(id)
}

class EndorsementEntity(id: EntityID<String>) : AbstractEntity<Endorsement>(id) {
    companion object : AbstractEntityClass<String, Endorsement, EndorsementEntity>(Endorsements)

    var profile: ProfileEntity by ProfileEntity referencedOn Endorsements.profile
    var campaign: CampaignEntity by CampaignEntity referencedOn Endorsements.campaign
    var type: ActionType by Endorsements.type
    var public: Boolean by Endorsements.public
    var createdAt: DateTime by Endorsements.createdAt

    override fun create(source: Endorsement): AbstractEntity<Endorsement> {
        profile = ProfileEntity[source.profile.id]
        campaign = CampaignEntity[source.campaign.id]
        type = source.type
        public = source.public
        createdAt = DateTime.now()
        return update(source)
    }

    override fun update(source: Endorsement): EndorsementEntity {
        return this
    }

    override fun toDomainType(): Endorsement {
        return Endorsement(
            id = id.value,
            profile = profile.toDomainType(),
            campaign = campaign.toDomainType(),
            type = type,
            public = public
        )
    }
}
