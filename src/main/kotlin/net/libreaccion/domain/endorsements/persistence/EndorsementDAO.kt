package net.libreaccion.domain.endorsements.persistence

import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.accounts.persistence.Profiles
import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.domain.campaigns.persistence.Campaigns
import net.libreaccion.domain.endorsements.model.Endorsement
import net.libreaccion.support.pagination.Cursor
import net.libreaccion.support.pagination.Page
import net.libreaccion.support.pagination.paging
import net.libreaccion.support.pagination.toPage
import net.libreaccion.support.persistence.TransactionSupport
import org.jetbrains.exposed.dao.id.EntityID

class EndorsementDAO : TransactionSupport() {
    fun saveOrUpdate(endorsement: Endorsement): Endorsement = transaction {
        EndorsementEntity.saveOrUpdate(endorsement.id, endorsement)
    }

    fun list(
        campaign: Campaign,
        cursor: Cursor
    ): Page<Endorsement> = transaction {
        EndorsementEntity.find {
            Endorsements.campaign eq EntityID(campaign.id, Campaigns)
        }.paging(cursor, Endorsements).toPage(cursor)
    }

    fun list(
        profile: Profile,
        cursor: Cursor
    ): Page<Endorsement> = transaction {
        EndorsementEntity.find {
            Endorsements.profile eq EntityID(profile.id, Profiles)
        }.paging(cursor, Endorsements).toPage(cursor)
    }
}
