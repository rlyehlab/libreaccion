package net.libreaccion.domain.endorsements

import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.domain.endorsements.model.ActionType
import net.libreaccion.domain.endorsements.model.Endorsement
import net.libreaccion.domain.endorsements.persistence.EndorsementDAO
import net.libreaccion.support.pagination.Cursor
import net.libreaccion.support.pagination.Page

class EndorsementService(private val endorsementDAO: EndorsementDAO) {
    fun endorse(
        profile: Profile,
        campaign: Campaign,
        type: ActionType,
        public: Boolean = true
    ): Endorsement {
        val endorsement = Endorsement.new(
            profile = profile,
            campaign = campaign,
            type = type,
            public = public
        )
        return endorsementDAO.saveOrUpdate(endorsement)
    }

    fun listPublic(
        campaign: Campaign,
        cursor: Cursor
    ): Page<Endorsement> {
        return endorsementDAO.list(campaign, cursor)
    }

    fun list(
        profile: Profile,
        cursor: Cursor
    ): Page<Endorsement> {
        return endorsementDAO.list(profile, cursor)
    }
}
