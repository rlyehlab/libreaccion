package net.libreaccion.domain.endorsements.model

/** Actions that users can perform on a campaign.
 * The platform will encourage real commitment on a campaign.
 */
enum class ActionType {
    SIGNATURE,
    TWEET,
    EMAIL,
    PROTEST
}
