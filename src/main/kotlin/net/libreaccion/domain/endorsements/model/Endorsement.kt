package net.libreaccion.domain.endorsements.model

import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.support.IdGenerator.randomId

/** Represents an individual endorsement to a campaign.
 */
data class Endorsement(
    val id: String,
    val profile: Profile,
    val campaign: Campaign,
    val type: ActionType,
    val public: Boolean
) {
    companion object {
        fun new(
            profile: Profile,
            campaign: Campaign,
            type: ActionType,
            public: Boolean
        ): Endorsement = Endorsement(
            id = randomId(),
            profile = profile,
            campaign = campaign,
            type = type,
            public = public
        )
    }
}
