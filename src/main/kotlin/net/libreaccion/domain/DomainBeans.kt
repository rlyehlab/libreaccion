package net.libreaccion.domain

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import net.libreaccion.domain.campaigns.CampaignService
import net.libreaccion.domain.endorsements.EndorsementService
import net.libreaccion.domain.store.FileSystemDataStore
import net.libreaccion.support.ObjectMapperFactory
import org.springframework.context.support.beans
import java.io.File

object DomainBeans {
    fun beans() = beans {
        val mainConfig: Config = ConfigFactory.defaultApplication().resolve()

        bean("CamelCaseObjectMapper") {
            ObjectMapperFactory.camelCaseMapper
        }

        bean("SnakeCaseObjectMapper") {
            ObjectMapperFactory.snakeCaseMapper
        }

        bean<CampaignService>()
        bean<EndorsementService>()

        // Data store
        bean {
            val dataStoreConfig: Config = mainConfig.getConfig("app.data-store")

            when (val dataStoreType: String = dataStoreConfig.getString("type")) {
                "fileSystem" -> FileSystemDataStore(
                    documentDAO = ref(),
                    directory = File(dataStoreConfig.getString("directory"))
                )
                else -> throw RuntimeException("data store not supported: $dataStoreType")
            }
        }
    }
}
