package net.libreaccion

import io.javalin.Javalin
import io.javalin.plugin.rendering.template.JavalinThymeleaf
import net.libreaccion.application.ApplicationBeans
import net.libreaccion.application.Endpoints
import net.libreaccion.datasource.DataSourceBeans
import net.libreaccion.domain.DomainBeans
import net.libreaccion.security.SecurityBeans
import net.libreaccion.security.SecurityProvider
import net.libreaccion.server.ServerBeans
import net.libreaccion.server.ServerConfig
import net.libreaccion.support.persistence.DataSourceInitializer
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.getBean
import org.springframework.context.annotation.AnnotationConfigApplicationContext

class Application {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(Application::class.java)
        lateinit var context: AnnotationConfigApplicationContext
    }

    private val beanDefinitions = listOf(
        ConfigBeans.beans(),
        DataSourceBeans.beans(),
        DomainBeans.beans(),
        ApplicationBeans.beans(),
        SecurityBeans.beans(),
        ServerBeans.beans()
    )

    init {
        val start = System.currentTimeMillis()

        logger.info("starting application")
        org.h2.engine.Mode.getRegular().alterTableModifyColumn = true

        context = AnnotationConfigApplicationContext().apply {
            beanDefinitions.forEach { beanDefinitionDsl ->
                beanDefinitionDsl.initialize(this)
            }
            refresh()
        }

        logger.info("initializing data source")
        initDataSource(context.getBean())

        logger.info("configuring web server")
        val securityProvider: SecurityProvider = context.getBean()
        val serverConfig: ServerConfig = context.getBean()
        val endpoints: Endpoints = context.getBean()
        JavalinThymeleaf.configure(context.getBean())
        val app = Javalin.create { config ->
            config.apply {
                endpoints.configure(this)
                securityProvider.configure(this)
                sessionHandler { context.getBean() }
                showJavalinBanner = false
            }
        }.start(serverConfig.host, serverConfig.port)
        Runtime.getRuntime().addShutdownHook(Thread { app.stop() })

        logger.info("initializing security")
        securityProvider.initialize(app)
        logger.info("registering endpoints")
        endpoints.register(app)
        logger.info("application ready (${System.currentTimeMillis() - start} ms)")
    }

    private fun initDataSource(dataSourceInitializer: DataSourceInitializer) {
        dataSourceInitializer.dropIfRequired(if (dataSourceInitializer.isTest) {
            "/db/drop.h2.sql"
        } else {
            "/db/drop.sql"
        })
        if (!dataSourceInitializer.isTest) {
            transaction {
                exec("USE libreaccion;")
            }
        }
        if (dataSourceInitializer.isTest) {
            dataSourceInitializer.initAuto("/db/drop.h2.sql")
        } else {
            dataSourceInitializer.initIfRequired("classpath:/db/*.sql")
        }
    }
}

fun main(args: Array<String>) {
    Application()
}
