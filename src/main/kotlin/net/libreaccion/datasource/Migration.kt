package net.libreaccion.datasource

import net.libreaccion.support.persistence.TransactionSupport

/** Represents a data source migration.
 *
 * Migrations run in their own transaction.
 *
 * Migrations MUST BE idempotent, they must write changes only once even if they are executed several times.
 */
abstract class Migration : TransactionSupport() {
    /** Returns the execution order of this migration relative to all other migrations.
     * Lower values executes first.
     */
    open val order: Int = 0

    /** Returns the environments this migration will apply to.
     */
    abstract val scope: List<String>

    /** Provides a description for this migration.
     */
    abstract val description: String

    /** Runs this migration. This method will be always called within a valid transaction.
     */
    abstract fun migrate()

    /** Executes this migration within a transaction.
     */
    fun execute() = transaction {
        migrate()
    }
}
