package net.libreaccion.datasource

import com.zaxxer.hikari.HikariDataSource
import net.libreaccion.datasource.migrations.CreateTestCampaign
import net.libreaccion.datasource.migrations.CreateTestEndorsements
import net.libreaccion.datasource.migrations.InitTestUsers
import net.libreaccion.domain.accounts.persistence.*
import net.libreaccion.domain.campaigns.persistence.*
import net.libreaccion.domain.endorsements.persistence.EndorsementDAO
import net.libreaccion.domain.endorsements.persistence.Endorsements
import net.libreaccion.domain.sessions.persistence.SessionDAO
import net.libreaccion.domain.sessions.persistence.Sessions
import net.libreaccion.domain.store.persistence.DocumentDAO
import net.libreaccion.domain.store.persistence.Documents
import net.libreaccion.domain.store.persistence.Locks
import net.libreaccion.support.persistence.DataSourceInitializer
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.springframework.context.support.beans
import javax.sql.DataSource

object DataSourceBeans {
    fun beans() = beans {
        // Tables
        bean { Accounts }
        bean { Organizations }
        bean { AccountOrganizations }
        bean { Profiles }
        bean { Campaigns }
        bean { Targets }
        bean { CampaignsTargets }
        bean { Documents }
        bean { Locks }
        bean { Sessions }
        bean { Endorsements }

        // DAOs
        bean<CampaignDAO>()
        bean<AccountDAO>()
        bean<OrganizationDAO>()
        bean<ProfileDAO>()
        bean<DocumentDAO>()
        bean<SessionDAO>()
        bean<EndorsementDAO>()
        bean<TargetDAO>()

        // Migrations (they are executed in this order)
        bean<InitTestUsers>()
        bean<CreateTestCampaign>()
        bean<CreateTestEndorsements>()

        bean {
            Database.connect(
                datasource = ref<DataSource>()
            )
        }

        bean {
            val dataSourceConfig: DataSourceConfig = ref()

            HikariDataSource().apply {
                jdbcUrl = dataSourceConfig.url
                username = dataSourceConfig.user
                password = dataSourceConfig.password
                driverClassName = dataSourceConfig.driver
            }
        }

        bean {
            DataSourceInitializer(
                dataSourceConfig = ref(),
                tables = provider<Table>().toList(),
                migrations = provider<Migration>().toList()
            )
        }
    }
}
