package net.libreaccion.datasource.migrations

import net.libreaccion.Environment.LOCAL_DEV
import net.libreaccion.datasource.Migration
import net.libreaccion.datasource.migrations.InitTestUsers.Companion.TEST_ORG_NAME
import net.libreaccion.domain.accounts.model.Organization
import net.libreaccion.domain.accounts.persistence.OrganizationDAO
import net.libreaccion.domain.accounts.persistence.ProfileDAO
import net.libreaccion.domain.campaigns.CampaignService
import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.domain.campaigns.model.Target
import net.libreaccion.domain.campaigns.persistence.CampaignEntity
import net.libreaccion.domain.campaigns.persistence.Campaigns
import net.libreaccion.domain.store.DataStore
import net.libreaccion.domain.store.model.Document
import java.io.InputStream

class CreateTestCampaign(
    private val campaignService: CampaignService,
    private val organizationDAO: OrganizationDAO,
    private val profileDAO: ProfileDAO,
    private val dataStore: DataStore
) : Migration() {

    companion object {
        private const val CAMPAIGN_TITLE: String = "¡PAREN EL NEGOCIO CON CHINA PARA SER SUS FABRICANTES DE CERDOS Y DE NUEVAS PANDEMIAS!"
        const val FRIENDLY_ID: String = "paren-las-granjas-de-cerdos"
    }

    override val order: Int = 1
    override val description: String = "Creates test campaigns for local development"
    override val scope: List<String> = listOf(LOCAL_DEV)

    override fun migrate() {
        val organization: Organization? = organizationDAO.findByName(TEST_ORG_NAME)

        val campaign: Campaign = CampaignEntity.find {
            Campaigns.title eq CAMPAIGN_TITLE
        }.firstOrNull()?.toDomainType() ?: Campaign.new(
            coordinator = profileDAO.findByUser("seykron")!!,
            organization = organization,
            friendlyId = FRIENDLY_ID,
            title = CAMPAIGN_TITLE,
            description = """
                Si quiere crear pandemias, entonces construya granjas industriales. La sociedad está buscando una
                vacuna para COVID-19 y lo más probable es que la encuentre. Pero ¿de qué sirve crear vacunas contra
                virus si no frenamos la raíz de el problema, es decir la construcción de los sistemas industriales
                intensivos que están creando estos virus?

                Vivimos sumergidos una pandemia mundial que nació en China. COVID-19 es una enfermedad zoonótica (salto
                genético de animal a humano) y está directamente relacionada con la forma en que los humanos crían,
                consumen y comercializan animales. Cuando, se busca producir carne animal en gran cantidad se crean
                granjas industriales donde millones de animales nacen, crecen en espacios sucios, sin tener acceso al
                movimiento, y por consecuencia tienden a enfermarse. Aquí se crea el ambiente perfecto para la
                propagación de las llamadas supercepas de gripe. Además, la carne que se vende al consumidor puede
                contener bacterias resistentes a los antibióticos causando infecciones resistentes a los fármacos en
                las personas.

                **En China están sufriendo brotes de Peste Porcina Africana que surgió entre el 2018 y 2019 (PPA), la
                misma está arruinando los medios de vida de muchos pequeños granjeros y sacrificando la vida de
                aproximadamente 400 millones de cerdos, que han decidido enterrar vivos**. Los encubrimientos de China
                han permitido la propagación del ganado hoy amenazando a los criadores de cerdo de todo el mundo. Desde
                el brote en China, la peste porcina africana a estallado en 10 países de Asia.
            """.trimIndent()
        )

        val image: Document = Document.new(
            name = "test-campaign-01.jpeg",
            type = "image/jpeg",
            size = 376832,
            meta = mapOf("test" to "true")
        )
        val content: InputStream = Thread.currentThread().contextClassLoader
            .getResourceAsStream("content/test-campaign-01.jpeg")!!
        val media: Document = dataStore.save(image, content)
        val target: Target = Target.new("Felipe Sola", "Canciller", contactEmail = "cancilleria@argentina.gob.ar")
        val campaignWithMedia: Campaign = campaign.copy(
            media = media
        )

        campaignService.saveOrUpdate(campaignWithMedia)
        campaignService.changeTargets(campaignWithMedia, listOf(target))
    }
}
