package net.libreaccion.datasource.migrations

import net.libreaccion.Environment.LOCAL_DEV
import net.libreaccion.datasource.Migration
import net.libreaccion.domain.accounts.model.Account
import net.libreaccion.domain.accounts.model.Organization
import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.security.Roles
import net.libreaccion.domain.accounts.persistence.AccountDAO
import net.libreaccion.domain.accounts.persistence.OrganizationDAO
import net.libreaccion.domain.accounts.persistence.ProfileDAO
import net.libreaccion.security.BcryptPasswordService

class InitTestUsers(
    private val accountDAO: AccountDAO,
    private val organizationDAO: OrganizationDAO,
    private val profileDAO: ProfileDAO
) : Migration() {

    companion object {
        const val TEST_ORG_NAME = "R'lyeh Lab"
    }

    private val passwordEncoder = BcryptPasswordService()

    private var testOrganization = Organization.new(
        name = TEST_ORG_NAME,
        contactEmail = "contacto@rlab.be",
        public = true
    )
    private val testAccounts: MutableList<Account> = mutableListOf(
        Account.new(
            organizations = listOf(testOrganization),
            email = "test1@rlab.be",
            password = passwordEncoder.hash("test123")
        ),
        Account.new(
            organizations = emptyList(),
            email = "test2@rlab.be",
            password = passwordEncoder.hash("test123")
        ),
        Account.new(
            organizations = emptyList(),
            email = "test3@rlab.be",
            password = passwordEncoder.hash("test123")
        )
    )
    private val testProfiles: MutableList<Profile> = mutableListOf(
        Profile.new(
            account = testAccounts[0],
            userName = "seykron",
            displayName = "seyk"
        ),
        Profile.new(
            account = testAccounts[1],
            userName = "saico",
            displayName = "ElSaico"
        ),
        Profile.new(
            account = testAccounts[2],
            userName = "solar",
            displayName = "SolarData"
        )
    )

    override val description: String = "Creates test users and organizations for local development"

    override val scope: List<String> = listOf(LOCAL_DEV)

    override fun migrate() {
        testOrganization = organizationDAO.findByName(TEST_ORG_NAME)
            ?: organizationDAO.saveOrUpdate(testOrganization)

        testAccounts.replaceAll { account ->
            val existingAccount = accountDAO.findByEmail(
                account.email
            )
            existingAccount?.let {
                accountDAO.saveOrUpdate(account.copy(id = existingAccount.id))
            } ?: accountDAO.saveOrUpdate(account)
        }

        accountDAO.assignRoles(testAccounts[0], testOrganization, listOf(Roles.ADMIN, Roles.USER))
        accountDAO.assignRoles(testAccounts[1], testOrganization, listOf(Roles.USER))

        testProfiles.replaceAll { profile ->
            val account: Account = accountDAO.findByEmail(profile.account.email)!!
            profileDAO.findByAccount(account)?.let { existingProfile ->
                profileDAO.saveOrUpdate(profile.copy(id = existingProfile.id))
            } ?: profileDAO.saveOrUpdate(
                profile.copy(account = account)
            )
        }
    }
}
