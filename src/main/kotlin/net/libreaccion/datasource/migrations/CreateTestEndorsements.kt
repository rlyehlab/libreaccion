package net.libreaccion.datasource.migrations

import net.libreaccion.Environment.LOCAL_DEV
import net.libreaccion.datasource.Migration
import net.libreaccion.datasource.migrations.CreateTestCampaign.Companion.FRIENDLY_ID
import net.libreaccion.domain.accounts.model.Profile
import net.libreaccion.domain.accounts.persistence.ProfileDAO
import net.libreaccion.domain.campaigns.model.Campaign
import net.libreaccion.domain.campaigns.persistence.CampaignDAO
import net.libreaccion.domain.endorsements.EndorsementService
import net.libreaccion.domain.endorsements.model.ActionType
import net.libreaccion.domain.endorsements.model.Endorsement
import net.libreaccion.support.pagination.Cursor
import net.libreaccion.support.pagination.Page

class CreateTestEndorsements(
    private val campaignDAO: CampaignDAO,
    private val profileDAO: ProfileDAO,
    private val endorsementService: EndorsementService
) : Migration() {

    override val order: Int = 2
    override val description: String = "Endorses a test campaign for local development"
    override val scope: List<String> = listOf(LOCAL_DEV)

    override fun migrate() {
        val campaign: Campaign = campaignDAO.get(FRIENDLY_ID)
        val endorsements: Page<Endorsement> = endorsementService.listPublic(campaign, Cursor.new(0, 30))

        if (endorsements.items.isEmpty()) {
            val seykron: Profile = profileDAO.findByUser("seykron") ?: throw RuntimeException("profile not found")
            val saico: Profile = profileDAO.findByUser("saico") ?: throw RuntimeException("profile not found")
            val solar: Profile = profileDAO.findByUser("solar") ?: throw RuntimeException("profile not found")
            endorsementService.endorse(seykron, campaign, ActionType.TWEET)
            endorsementService.endorse(saico, campaign, ActionType.EMAIL)
            endorsementService.endorse(solar, campaign, ActionType.SIGNATURE)
        }
    }
}
