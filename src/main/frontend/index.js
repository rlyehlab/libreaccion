import Vue from 'vue/dist/vue.js';
import Router from 'vue-router';
import VueI18n from 'vue-i18n';
import App from './App';
import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min';
import 'font-awesome/css/font-awesome.min.css';
import './assets/css/icon.css';
import router from './modules/router';
import { i18n, configureLocale } from './modules/i18n';
import VMarkdown from './modules/v-markdown';
import { initialize } from './modules/http';

Vue.config.productionTip = false;

import VueLogger from 'vuejs-logger';

Vue.use(VueLogger, {
    isEnabled: true,
    logLevel: 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: false,
    separator: '|',
    showConsoleColors: true
});
Vue.use(Router);
Vue.use(VueI18n);
Vue.use(VMarkdown);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    components: { App },
    template: '<App/>',
    router,
    i18n: i18n(),
    async beforeMount() {
        initialize();
    },
    async mounted() {
        this.currentUser = undefined;
        await configureLocale(this, this.currentUser, this.$route.query.locale);
    }
});
