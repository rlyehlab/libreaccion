module.exports = {
    env: {
        es6: true,
        node: true,
        jest: true
    },
    extends: [
        'plugin:vue/recommended', // Vue specific rules (i.e. no v-models on divs)
        'eslint:recommended', // default javascript best-practice rules
        'prettier/vue', // Vue specific prettier formatting
        'plugin:prettier/recommended' // use eslint-plugin-prettier and eslint-config-prettier
    ],
    plugins: [],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly'
    },
    parserOptions: {
        ecmaVersion: 6,
        parser: 'babel-eslint'
    },
    rules: {
        'no-prototype-builtins': 'off',
        'vue/attributes-order': 'off',
        'vue/name-property-casing': ['warn', 'PascalCase'],
        'vue/require-prop-types': 'error',
        'vue/require-default-prop': 'error',
        'filenames/match-exported': 'off',
        'no-useless-escape': 'warn',
        'no-unused-vars': ['error', { args: 'none', ignoreRestSiblings: true }],
        'require-atomic-updates': 'off',
        'prettier/prettier': [
            'error',
            { singleQuote: true, tabWidth: 4, trailingComma: 'none' }
        ],
        'vue/no-mutating-props': 'warn',
        'vue/custom-event-name-casing': 'warn',
        'vue/no-unused-properties': [
            'error',
            {
                groups: ['props', 'data', 'computed', 'methods']
            }
        ]
    }
};
