import { get, patch } from '../../modules/http';

export default {
    async getAttribute(name) {
        const session = await get('/session');
        return session.attributes[name] || null;
    },

    async setAttribute(name, value) {
        return this.setAttributes({ [name]: value });
    },

    async setAttributes(attribs) {
        await patch('/session/attributes', attribs);
        return attribs;
    }
};
