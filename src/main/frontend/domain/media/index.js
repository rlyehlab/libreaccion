import { MediaManager as MediaManagerClass } from './MediaManager';
const UPLOAD_ENDPOINT = '/content';

export const MediaManager = new MediaManagerClass(UPLOAD_ENDPOINT);
