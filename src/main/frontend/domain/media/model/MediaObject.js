/** Represents any kind of media content.
 */
export class MediaObject {
    constructor({ id, name, url, type, size }) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.type = type;
        this.size = size;
    }

    get baseType() {
        return this.type.substr(0, this.type.indexOf('/'));
    }
}
