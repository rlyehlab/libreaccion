import { put, get } from '../../modules/objectCache';
import axios from 'axios';
import { AsyncRequest, RequestStatus } from "../../modules/AsyncRequest";
import { MediaObject } from "./model/MediaObject";

/** Map from request id to active request.
 * @type {Map<string, ActiveRequest>}
 */
const activeRequests = new Map();

function generateId(file) {
    const { name, type, size } = file;
    return `${name}!${type}!${size}`;
}

class ActiveRequest {
    /**
     *
     * @param {MediaObject} localMediaObject
     * @param {AsyncRequest} asyncRequest
     */
    constructor({ localMediaObject, asyncRequest }) {
        this._localMediaObject = localMediaObject;
        this._asyncRequest = asyncRequest;

        const activeRequest = this;
        asyncRequest.onSuccess(response => {
            activeRequest._mediaObject = put(new MediaObject(response.data));
        });
    }

    cancel() {
        this._asyncRequest.cancel();
    }

    revoke() {
        URL.revokeObjectURL(this._localMediaObject.url)
    }

    get inProgress() {
        return this._asyncRequest.status === RequestStatus.PENDING;
    }

    get done() {
        return this._asyncRequest.status === RequestStatus.DONE;
    }

    get valid() {
        return this.inProgress || this.done;
    }

    get mediaObject() {
        return this._mediaObject || this._localMediaObject;
    }
}

export class MediaManager {
    /** Constructs an upload manager to upload files to the specified endpoint.
     * @param {string} contentEndpoint Base endpoint to upload and read content.
     */
    constructor(contentEndpoint) {
        this._contentEndpoint = contentEndpoint;
    }

    /** Checks whether the specified media object is being uploaded.
     * @param {string} id Content identifier to verify.
     * @return {boolean} true if the media object is being uploaded, false otherwise.
     */
    inProgress(id) {
        return activeRequests.has(id) && activeRequests.get(id).inProgress;
    }

    /** Returns a MediaObject by id.
     * @param {string} id Content identifier.
     * @return {Promise<MediaObject>} a promise to the media object, or null if it doesn't exist.
     */
    async findById(id) {
        if (activeRequests.has(id) && activeRequests.get(id).done) {
            return activeRequests.get(id).mediaObject;
        } else if (get(id)) {
            return get(id);
        } else {
            const response = await axios.get(`${this._contentEndpoint}/${id}/meta`);
            return response.status === 200 && put(new MediaObject(response.data));
        }
    }

    /** Cancels the upload of a file if the upload is still in progress.
     * @param {string} id Related content identifier to cancel the upload.
     */
    cancelUpload(id) {
        if (activeRequests.has(id)) {
            activeRequests.get(id).cancel();
            activeRequests.delete(id);
        }
    }

    /** Starts an upload and returns the request.
     * @param {File} file File to upload.
     * @returns {MediaObject}
     */
    upload(file) {
        const id = generateId(file);
        const existingRequest = activeRequests.get(id);

        if (existingRequest && existingRequest.valid) {
            return existingRequest.mediaObject;
        } else if (existingRequest) {
            existingRequest.revoke();
            activeRequests.delete(id);
        }

        const asyncRequest = this._makeRequest(id, file);
        const localMediaObject = put(
            new MediaObject({
                id,
                name: file.name,
                url: URL.createObjectURL(file),
                type: file.type,
                size: file.size
            })
        );
        const activeRequest = new ActiveRequest({ localMediaObject, asyncRequest })
        activeRequests.set(id, activeRequest);

        return activeRequest.mediaObject;
    }

    _makeRequest(id, file) {
        const data = new FormData();
        const { name, type, size } = file;
        data.set('name', name);
        data.set('type', type);
        data.set('size', size);
        data.set('file', file);

        const cancelToken = axios.CancelToken.source();
        const innerRequest = axios.post(this._contentEndpoint, data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            cancelToken: cancelToken.token
        });
        return new AsyncRequest({ id, innerRequest, cancelToken });
    }
}
