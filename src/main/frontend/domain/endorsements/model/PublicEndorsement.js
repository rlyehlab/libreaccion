import { put } from '../../../modules/objectCache';
import { Profile } from '../../accounts/model/Profile';

export class PublicEndorsement {
    constructor({ id, profile, campaignId, type, createdAt }) {
        this.id = id;
        this.profile = put(new Profile(profile));
        this.campaignId = campaignId;
        this.type = type;
        this.createdAt = createdAt;
    }
}
