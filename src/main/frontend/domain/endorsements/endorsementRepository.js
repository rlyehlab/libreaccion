import { post } from '../../modules/http';
import { put } from '../../modules/objectCache';
import { PublicEndorsement } from './model/PublicEndorsement';
import { PageRequest } from '../../modules/paging';

function newEndorsement(model) {
    return put(new PublicEndorsement(model));
}

export default {
    async endorse(campaignId, type, isPublic) {
        return newEndorsement(
            await post(`/api/campaigns/${campaignId}/endorsements`, {
                type,
                public: isPublic
            })
        );
    },

    async listPublic(campaignId) {
        const page = await new PageRequest({
            servicePath: `/api/campaigns/${campaignId}/endorsements`
        }).fetch();

        return page.items.map(newEndorsement);
    }
};
