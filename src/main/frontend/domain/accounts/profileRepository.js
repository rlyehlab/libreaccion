import { get } from '../../modules/http';
import { put } from '../../modules/objectCache';
import { Profile } from './model/Profile';

function newProfile(model) {
    return put(new Profile(model));
}

export default {
    async current() {
        const session = await get('/session');
        return newProfile(session.profile);
    }
};
