import { put } from '../../../modules/objectCache';
import { MediaObject } from '../../media/model/MediaObject';

/** Represents a user or organization profile that can be placed
 * inline in the middle of other content.
 */
export class Profile {
    constructor({ id, userName, displayName, photo, updatedAt }) {
        this.id = id;
        this.userName = userName;
        this.displayName = displayName;
        if (photo) {
            this.photo = put(new MediaObject(photo));
        }
        this.updatedAt = updatedAt;
    }

    update(other) {
        this.userName = other.userName;
        this.displayName = other.displayName;
        this.photo = other.photo;
    }
}
