import { get } from '../../modules/http';
import { PageRequest } from '../../modules/paging';
import sessionRepository from '../session/sessionRepository';
import endorsementRepository from '../endorsements/endorsementRepository';
import { put } from '../../modules/objectCache';
import { Campaign } from './model/Campaign';
import { DraftCampaign } from './model/DraftCampaign';
import { v4 as uuid } from 'uuid';

async function newCampaign(model, withEndorsements = false) {
    const endorsements = withEndorsements
        ? await endorsementRepository.listPublic(model.id)
        : [];
    return put(
        new Campaign({
            ...model,
            support: {
                total: endorsements.length,
                endorsements
            }
        })
    );
}

const DRAFT_CAMPAIGN_ATTR = 'Campaign::currentDraft';

export default {
    async getById(id) {
        return await newCampaign(await get(`/api/campaigns/${id}`), true);
    },

    async listHome() {
        const page = await new PageRequest({
            servicePath: '/api/campaigns'
        }).fetch();
        return await Promise.all(page.items.map(newCampaign));
    },

    async currentDraft() {
        const draft = await sessionRepository.getAttribute(DRAFT_CAMPAIGN_ATTR);
        if (draft) {
            return put(new DraftCampaign(JSON.parse(draft)));
        } else {
            return put(new DraftCampaign({ id: uuid() }));
        }
    },

    async saveDraft(draft) {
        draft.updatedAt = new Date();
        await sessionRepository.setAttribute(
            DRAFT_CAMPAIGN_ATTR,
            JSON.stringify(draft)
        );
        return draft;
    }
};
