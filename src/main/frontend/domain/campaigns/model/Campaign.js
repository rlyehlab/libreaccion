import { put } from '../../../modules/objectCache';
import { MediaObject } from '../../media/model/MediaObject';
import { Profile } from '../../accounts/model/Profile';

export class Campaign {
    constructor({
        id,
        friendlyId,
        title,
        description,
        media,
        coordinator,
        targets,
        support,
        createdAt,
        updatedAt
    }) {
        this.id = id;
        this.friendlyId = friendlyId;
        this.title = title;
        this.description = description;
        this.media = put(new MediaObject(media));
        this.coordinator = put(new Profile(coordinator));
        this.targets = targets.map((target) => put(new Profile(target)));

        const endorsements = support.endorsements.map((endorsement) => ({
            ...endorsement,
            profile: put(new Profile(endorsement.profile))
        }));
        this.support = {
            ...support,
            endorsements
        };
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    update(other) {
        this.friendlyId = other.friendlyId;
        this.title = other.title;
        this.description = other.description;
    }
}
