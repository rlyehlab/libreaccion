import { MediaManager } from '../../media';
import { put } from "../../../modules/objectCache";
import { MediaObject } from "../../media/model/MediaObject";

export const Steps = {
    SELECT_TITLE: {
        view: 'selectTitle',
        position: 0,
        validate(createCampaign) {
            return !!createCampaign.title;
        }
    },
    SELECT_TARGETS: {
        view: 'selectTargets',
        position: 1,
        validate(createCampaign) {
            return createCampaign.selectedTargets.length > 0;
        }
    },
    WRITE_DESCRIPTION: {
        view: 'writeDescription',
        position: 2,
        validate(createCampaign) {
            return !!createCampaign.description;
        }
    },
    SELECT_MEDIA: {
        view: 'selectMedia',
        position: 3,
        validate(createCampaign) {
            return true;
        }
    }
};

const MOCK_TARGETS = [
    {
        name: 'Juan Perez',
        photo: undefined
    }
];

/** Returns the list of steps sorted by position.
 * @returns {[{ view: string, position: number, validate: function }]} A list of steps.
 */
function sortedSteps() {
    const sortedSteps = Object.keys(Steps).sort((step1, step2) => {
        if (Steps[step1].position > Steps[step2].position) {
            return 1;
        } else if (Steps[step1].position < Steps[step2].position) {
            return -1;
        }
        return 0;
    });
    return sortedSteps.map((stepName) => Steps[stepName]);
}

/** View model to manage the Create Campaign wizard state.
 */
export class DraftCampaign {
    constructor({
        id,
        stepName = 'SELECT_TITLE',
        title,
        description,
        media,
        selectedTargets = [],
        updatedAt
    }) {
        this.id = id;
        this.stepName = stepName;
        this.title = title;
        this.description = description;
        this.selectedTargets = selectedTargets;
        this.step =
            sortedSteps().find((step) => step.view === this.stepName) ||
            Steps.SELECT_TITLE;
        this.media = media && put(new MediaObject(media));
        this._allTargets = MOCK_TARGETS;
        this.updatedAt = updatedAt || new Date();
    }

    /** Moves the wizard to the next step, if possible.
     * It validates the current step and moves forward only if the validation succeeded.
     */
    nextStep() {
        if (!this.step.validate(this)) {
            return;
        }

        const nextStepPos = this.step.position + 1;
        const nextStep = sortedSteps().find((step) => {
            return step.position === nextStepPos;
        });
        this.step = nextStep;
        this.stepName = nextStep.view;
    }

    /** Displays the step at the specified position.
     *
     * All the previous steps should be completed and validated.
     *
     * @param {number} position Step position to jump to.
     */
    goToStep(position) {
        const candidateStep = sortedSteps().find((step) => {
            return step.position === position;
        });

        const canJump = sortedSteps().every(
            (step) =>
                (step.position < candidateStep.position &&
                    step.validate(this)) ||
                step.position >= candidateStep.position
        );

        if (!canJump) {
            return;
        }

        this.step = candidateStep;
        this.stepName = candidateStep.view;
    }

    async searchTargets(criteria) {
        const exists = this._allTargets.some(
            (target) => target.name === criteria
        );
        if (!exists) {
            this._allTargets.push({ name: criteria, photo: undefined });
        }

        return this._allTargets;
    }

    updateTargets(targets) {
        this.selectedTargets = targets
            .map((name) =>
                this._allTargets.find((target) => target.name === name)
            )
            .filter(Boolean);
    }

    updateMedia(file) {
        this.media = MediaManager.upload(file);
    }

    removeMedia() {
        if (this.media) {
            MediaManager.cancelUpload(this.media.id);
            this.media = null;
        }
    }

    /** If the media upload is still in progress, it cancels the upload, otherwise
     * it updates the campaign's media with the uploaded MediaObject.
     */
    async syncMedia() {
        if (!this.media) {
            return;
        }
        if (MediaManager.inProgress(this.media.id)) {
            this.removeMedia();
        } else {
            this.media = await MediaManager.findById(this.media.id);
        }
    }
}
