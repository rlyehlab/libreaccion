const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { EnvironmentPlugin } = require('webpack');

module.exports = {
    mode: 'development',
    entry: './index.js',
    context: path.resolve(__dirname),
    devtool: '#cheap-module-source-map',
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            },
            {
                test: /\.s(c|a)ss$/,
                use: [
                    {
                        loader: 'sass-loader',
                        options: {
                            implementation: require('sass'),
                            sassOptions: {
                                fiber: require('fibers'),
                                indentedSyntax: true // optional
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(htm|html)$/,
                loader: 'raw-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                loader: 'file-loader',
                options: {
                    outputPath: 'assets',
                    esModule: false
                }
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000',
                options: {
                    esModule: false
                }
            },
            {
                test: /\.ya?ml$/,
                type: 'json',
                use: 'yaml-loader'
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
        // Automatically generates the index file based on a template.
        new HtmlWebpackPlugin({
            template: 'index.html.base',
            inject: true,
            meta: {
                sha: process.env.GIT_COMMIT || 'localdev',
                version: require('../../../package.json').version,
                buildJob: process.env.BUILD_NUMBER || 'localbuild'
            },
            filename: 'index.html'
        }),
        new EnvironmentPlugin(['NODE_ENV', 'SERVER_BASE_URL'])
    ],
    // We write the output to the Java's classpath root directory. Once packed it will be
    // served by the backend. The configuration is in the WebConfig.kt class.
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, '../../../target/classes/frontend/'),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.vue']
    }
};
