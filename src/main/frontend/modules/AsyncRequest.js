import axios from "axios";
import { v4 as uuid} from 'uuid';

async function wait(milliseconds) {
    return new Promise((resolve => {
        setTimeout(resolve, milliseconds);
    }));
}

export const RequestStatus = {
    PENDING: 'pending',
    DONE: 'done',
    CANCELLED: 'cancelled',
    ERROR: 'error'
};

/** Represents an asynchronous HTTP request.
 * It allows to wait for the request with a polling-like strategy using the #wait() method.
 * Request can be cancelled using the #cancel() method.
 */
export class AsyncRequest {
    /** Constructs a new async request to wrap an Axios request.
     * @param {string} [id] Request id. If it's not specified it will be generated.
     * @param {Promise<AxiosResponse<any> | void>} innerRequest Axios HTTP request.
     * @param {CancelTokenSource} cancelToken Axios token to cancel the request.
     */
    constructor({ id, innerRequest, cancelToken }) {
        this.id = id || uuid();
        this.status = RequestStatus.PENDING;
        this._innerRequest = innerRequest;
        this._cancelToken = cancelToken;
        this.error = null;
        this.result = null;

        const asyncRequest = this;
        innerRequest.then(result =>
            asyncRequest._handleResult(result)
        ).catch(cause =>
            asyncRequest._handleError(cause)
        );
    }

    /** Adds a handler that's called only when the request ends successfully.
     * @param {function(object)} callback Receives the Axios HTTP response.
     * @returns {AsyncRequest} returns this request.
     */
    onSuccess(callback) {
        this._innerRequest.then(callback);
        return this;
    }

    /** Wait for this request to finish.
     *
     * If the milliseconds parameter is equal to or less than 0, it will wait until the request ends,
     * otherwise it will wait for the specified amount of milliseconds and then it will return.
     *
     * If this method is called after the request has ended, it will return true.
     *
     * @param {number} [milliseconds] Number of milliseconds to wait.
     * @returns {Promise<boolean>} Returns true if the request has ended, false otherwise.
     */
    async wait(milliseconds = 0) {
        if (this.status !== RequestStatus.PENDING) {
            return true;
        }
        if (milliseconds <= 0) {
            await this.execSync();
            return true;
        } else {
            await wait(milliseconds);
            return this.status !== RequestStatus.PENDING;
        }
    }

    /** Executes this request synchronously and returns the result.
     * If the request is cancelled, it returns undefined.
     * Any error is thrown synchronously.
     * @returns {Promise<*>} returns the Axios HTTP response.
     */
    async execSync() {
        if (this.status !== RequestStatus.PENDING) {
            throw new Error('Request already executed');
        }
        try {
            return this._handleResult(await this._innerRequest);
        } catch (cause) {
            this._handleError(cause);
            if (this.status === RequestStatus.ERROR) {
                throw cause;
            }
        }
    }

    cancel() {
        this._cancelToken.cancel();
    }

    _handleError(cause) {
        if (axios.isCancel(cause)) {
            this.status = RequestStatus.CANCELLED;
        } else {
            this.error = cause;
            this.status = RequestStatus.ERROR;
        }
    }

    _handleResult(result) {
        this.result = result;
        this.status = RequestStatus.DONE;
        return result;
    }
}
