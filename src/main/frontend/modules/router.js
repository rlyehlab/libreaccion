import Router from 'vue-router';
import Home from '../components/Home';
import CreateCampaign from '../domain/campaigns/CreateCampaign';
import ViewCampaign from '../domain/campaigns/ViewCampaign';
import Login from '../domain/accounts/Login';
import { checkPermissions } from './http';

const routes = [
    { path: '/', component: Home },
    {
        name: 'create-campaign',
        path: '/campaigns/new',
        component: CreateCampaign,
        meta: { requiresAuth: true }
    },
    {
        name: 'view-campaign',
        path: '/campaigns/:campaignId',
        component: ViewCampaign,
        props: true
    },
    { path: '/login', component: Login }
];

const router = new Router({
    routes,
    mode: 'history'
});

router.beforeEach((to, from, next) => {
    if (to.meta && to.meta.requiresAuth) {
        checkPermissions(to.path).then((allowed) => {
            if (allowed) {
                next();
            } else {
                next('/login');
            }
        });
    } else {
        next();
    }
});

export default router;
