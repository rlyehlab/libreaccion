import marked from 'marked';

function render(el, binding) {
    el.innerHTML = marked(binding.value);
}

/** Directive to render markdown content into HTML tags.
 * @param Vue Vue instance.
 */
export default function install(Vue) {
    Vue.directive('markdown', {
        bind: render,
        update: render
    });
}
