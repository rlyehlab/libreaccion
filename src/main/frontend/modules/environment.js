const ENV_NAME = 'JAVA_ENV';
const currentEnv = process.env[ENV_NAME] || 'localdev';

const Environment = {
    isLocalDev: currentEnv === 'localdev',
    isProd: currentEnv === 'production',
    serverUrl: process.env.SERVER_BASE_URL
};

export const isLocalDev = Environment.isLocalDev;
export const isProd = Environment.isProd;
export const serverUrl = Environment.serverUrl;
