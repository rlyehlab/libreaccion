import { get } from './http';

/** Represents a request to an endpoint that supports pagination.
 */
export class PageRequest {
    /** Constructs a page request by using the cursor URI returned by a previous call
     * to the underlying endpoint.
     * @param {string} cursorUrl Cursor returned by the paginated endpoint.
     * @returns {PageRequest}
     */
    static fromCursor(cursorUrl) {
        const url = new URL(`${location.origin}${cursorUrl}`);

        return new PageRequest({
            servicePath: url.pathname,
            offset: url.searchParams.get('offset'),
            limit: url.searchParams.get('limit')
        });
    }

    /** Constructs a new request to a paginated endpoint.
     * @param {string} servicePath Pathname of the endpoint to fetch.
     * @param {number} [offset] Current offset in the entire record set.
     * @param {number} [limit] Items per page.
     */
    constructor({ servicePath, offset = 0, limit = 30 }) {
        this.servicePath = servicePath;
        this.offset = offset;
        this.limit = limit;
    }

    /** Fetches the configured page from the paginated endpoint.
     * @returns {Promise<Page>}
     */
    async fetch() {
        const url = new URL(`${location.origin}${this.servicePath}`);
        url.searchParams.set('offset', this.offset);
        url.searchParams.set('limit', this.limit);
        return new Page(await get(url.toString()));
    }
}

/** Represents a page returned by a paginated endpoint.
 * It supports navigation through the pages.
 */
export class Page {
    /** Constructs a new page from a paginated endpoint result.
     * @param {string} [previous] Cursor URL to the previous page, if any.
     * @param {string} [next] Cursor URL to the next page, if any.
     * @param {object[]} items List of items in this page.
     * @param {number} total Total number of items in the record set.
     */
    constructor({ previous, next, items, total }) {
        this._previous = previous && PageRequest.fromCursor(previous);
        this._next = next && PageRequest.fromCursor(next);
        this.items = items;
        this.total = total;
    }

    /** Returns the previous page, or undefined if no previous page is available.
     * @returns {Promise<undefined|Page>}
     */
    async previous() {
        return this._previous && (await this._previous.fetch());
    }

    /** Returns the next page, or undefined if no next page is available.
     * @returns {Promise<undefined|Page>}
     */
    async next() {
        return this._next && (await this._next.fetch());
    }
}
