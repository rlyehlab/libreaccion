import axios from 'axios';

export async function get(path) {
    const { data: result } = await axios.get(path);
    return result;
}

export async function post(path, payload) {
    const { data: result } = await axios.post(path, payload);
    return result;
}

export async function put(path, payload) {
    const { data: result } = await axios.put(path, payload);
    return result;
}

export async function patch(path, payload) {
    const { data: result } = await axios.patch(path, payload);
    return result;
}

export async function remove(path) {
    const { data: result } = await axios.delete(path);
    return result;
}

export async function checkPermissions(endpoint) {
    const res = await fetch(endpoint, {
        redirect: 'manual'
    });
    return res.type !== 'opaqueredirect';
}

export function initialize() {
    const jwtToken = document.querySelector('meta[name=_jwt_token]').content;
    if (jwtToken) {
        axios.defaults.headers.common = {
            Authorization: `Bearer ${jwtToken}`
        };
    }
}
