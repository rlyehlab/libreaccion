/** Global store, used to store settings and ViewModels state.
 */
export default {
    set(key, value) {
        localStorage.setItem(key, value);
    },

    get(key) {
        return localStorage.getItem(key);
    },

    remove(key) {
        return localStorage.removeItem(key);
    },

    saveState(key, viewModel) {
        this.set(key, JSON.stringify(viewModel));
    },

    restoreState(key, viewModel, defaultState) {
        const jsonViewModel = this.get(key);

        Object.assign(
            viewModel,
            (jsonViewModel && JSON.parse(jsonViewModel)) || defaultState || {}
        );
        Object.keys(defaultState).forEach((propertyName) => {
            if (!viewModel.hasOwnProperty(propertyName)) {
                viewModel[propertyName] = defaultState[propertyName];
            }
        });

        return viewModel;
    }
};
