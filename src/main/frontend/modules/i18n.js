import VueI18n from 'vue-i18n';
import '@toast-ui/editor/dist/i18n/es-es';

const localeConfig = {
    supportedLocales: ['es'],
    fallbackLocale: 'es'
};

function loadMessages(locale) {
    return require(`../lang/${locale}.yaml`);
}

export function i18n() {
    return new VueI18n(localeConfig);
}

/** Configures the locale for a Vue component.
 *
 * It resolves the locale in the following order of precedence:
 *
 * 1. Finds the locale for the user.
 * 2. Uses the provided default locale.
 * 3. Uses the locale detected from the browser.
 * 4. Uses the global fallback locale.
 *
 * @param {object} component Vue component.
 * @param {object} [user] Current logged-in user, if any.
 * @param {string} defaultLocale Locale to use if the locale cannot be resolved.
 */
export async function configureLocale(component, user, defaultLocale) {
    try {
        const resolvedLocale =
            (user && user.locale) || defaultLocale || navigator.language;
        component.$i18n.setLocaleMessage(
            resolvedLocale,
            loadMessages(resolvedLocale)
        );
        component.$i18n.locale = resolvedLocale;
    } catch (err) {
        component.$i18n.setLocaleMessage(
            localeConfig.fallbackLocale,
            loadMessages(localeConfig.fallbackLocale)
        );
        component.$i18n.locale = localeConfig.fallbackLocale;
    }
}
