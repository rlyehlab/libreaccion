/** Object cache that supports updating items based on each entry's modification time. This
 * is an in-memory cache, entries will live as long as the page is not reloaded. This cache
 * is backed by a Map.
 *
 * The goal is to provide a single object to represent the same entity along the application.
 * Instead of coupling the Vue components passing an instance over the component graph, this cache
 * provides a transparent strategy to re-use model objects which share the same ids.
 *
 * By convention, cache entries must have two fields: id and updatedAt. The id is a unique
 * identifier used to store the entry in the cache. The updatedAt field is a marker field that will
 * be evaluated with a strict _equals_ to determine whether an entry is dirty and need to be updated.
 *
 * If a cache entry is dirty and it implements the _update(other)_ method, this cache will delegate
 * the update operation to the entry's _update()_ method, otherwise it will replace the existing entry
 * in the cache with the new state.
 */

const modelCache = new Map();

/** Retrieves an entry from the cache.
 * @param {string} id Required entry id.
 * @returns {object} the required entry, or null if it doesn't exist.
 */
export function get(id) {
    return modelCache.get(id) || null;
}

/** Pushes an entity into the cache.
 * If an entity with the same id exists and the state is different, it will update
 * the existing entry with the new state.
 *
 * @param {object} model Entry to store in the cache.
 * @returns {object} Returns the last entry state.
 */
export function put(model) {
    if (modelCache.has(model.id) && model.updatedAt) {
        const existing = modelCache.get(model.id);
        const dirty = existing.updatedAt !== model.updatedAt;
        if (dirty && existing.update) {
            existing.update(model);
        } else if (dirty) {
            Object.assign(existing, model);
        }
        existing.updatedAt = model.updatedAt;
    } else {
        modelCache.set(model.id, model);
    }
    return modelCache.get(model.id);
}
