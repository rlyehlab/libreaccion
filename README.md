# LibreAccion

Plataforma de desobediencia digital.

En estos tiempos en que las voces que gritan los mensajes más importantes son sistemáticamente ignoradas por los
medios hegemónicos, en que los canales institucionales son sólo maquillaje republicano para que parezca que
vivimos en democracia, la desobediencia digital es un canal legítimo para torcerle el brazo a los agentes del sistema
y lograr introducir una agenda que realmente represente intereses *comunes*.

LibreAccion es una alternativa libre y anticapitalista a change.org y a todas las plataformas digitales que a cambio de
un servicio nos fuerzan a entregar nuestros datos y comprometer nuestra privacidad para alimentar el negocio de la
especulación digital. Este un proyecto autogestionado por la comunidad, con una organización horizontal, y no recibirá
ningún tipo de financiamiento que condicione o fuerce las decisiones sobre el desarrollo.

Todas las organizaciones son bienvenidas a aportar su conocimiento de campo para que las acciones que se realicen en
esta plataforma sean lo más eficaces posibles.

## Documentación

La documentación se encuentra en el directorio `docs/` y consta de las siguientes secciones:

1. [Contrato social](docs/social_contract.md)
2. [Desarrollo](docs/development.md)
3. [Releases](docs/release.md) 
4. [Organización](docs/organization.md) 

## Cómo sumarse

Si sos una organización y te interesa utilizar LibreAcción pero hay cosas que faltan o se pueden mejorar, podés
escribirnos a libreaccion[@]riseup.net.

Si querés ayudarnos a desarrollar la palaforma, podés mirar la lista de Issues o comunicarte al email de contacto.
