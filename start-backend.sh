#!/bin/sh
mvn compile || exit 1
mvn exec:exec -Dexec.executable="java" \
  -Dexec.classpathScope=runtime \
  -Dexec.includeProjectDependencies=true \
  -Dexec.args="-classpath %classpath -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=0.0.0.0:1337 net.libreaccion.ApplicationKt" \
  || exit 1
